<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brand extends Model
{
    //
    protected $table = "brand";
    public function products(){
        return $this->hasMany('App\products','brand_id','id');
    }
}
