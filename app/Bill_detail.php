<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill_detail extends Model
{
    //
    protected $table = "bill_detail";

    public function bill(){
        return $this->belongsTo('App\bill','bill_id','id');
    }
    public function products(){
        return $this->belongsTo('App\products','pro_id','id');
    }
}
