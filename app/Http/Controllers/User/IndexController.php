<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\product_image;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\user;
use App\category;
use App\images;
use DB;
class IndexController extends Controller
{
    //---------user============
    function __construct()
    {
        $category = category::all();
        foreach($category as $row){
            $category_child = product_type::where('type',$row->id)->get();
            $row->category_child = $category_child;
        }

        //dd($category);
        view()->share(compact('category'));
    }
    public function getIndex()
    { 
        $product = DB::table('bill_detail') 
            ->select('pro_id', DB::raw('SUM(quantity)'))
            ->groupBy('pro_id')->orderBy(DB::raw('SUM(quantity)'), 'desc')->take(8)
            ->get()->pluck('pro_id');


        $product_hot = DB::table('products')
            ->whereIn('id', $product)
            ->get();

            // dd($product_hot);
        // $product_hot= products::get();
        $news =  DB::table('news')->orderBy('updated_at', 'desc')->take(3)->get();

        return view('user.home',compact('product_hot','news'));
    }
        // guitar
    public function getProducts($id)
    {
        $productsAll = DB::table('products')
                    ->join('product_type', 'products.protype_id', '=', 'product_type.id')
                    ->join('category', 'product_type.type', '=', 'category.id')
                    ->where('category.id','=',$id)
                    ->select('products.product_name','products.image','products.price','products.promotion','products.id')
                    ->paginate(8);
        
        $productsType = category::Where('id', $id)->get();
        return view('user.pages.products.products',compact('productsAll','productsType'));
    }

    public function getListProducts($id)
    {
        $products = product_type::where('id',$id)->get();
        $productsList= products::where('protype_id', $id)->paginate(8);
       // dd( $productsList);
        return view('user.pages.products.listproducts', compact('productsList','products'));
    }
    public function getDetailProducts($id)
    {
        $products_img = product_image::where('product_id',$id)->get();
        $productsDetail = products::where('id',$id)->first();
        // dd($productsDetail);
        return view('user.pages.products.detailproducts',compact('productsDetail','products_img'));
    }

            // khuyenmai
    public function getPromotion()
    {
        // $products= DB::table('products')->get()->pluck('promotion');
        $promotion= DB::table('products')->where('promotion','>', 0)->paginate(16);

        // dd($products);
        return view('user.pages.promotion', compact('promotion'));
    }
            // tintuc
    // public function getNews()
    // {
    //     return view('user.pages.news.news');
    // }

    public function search(Request $request){
        $key = $request->key;
        $order = ($request->order) ? $request->order : '';
        $status = ($request->status) ? $request->status : '';
        if($order !== ''){
            $product = products::where('product_name', 'like', '%' . $key . '%')
                ->orderBy($order, $status)
                ->paginate(16);
        }else{
            $product = products::where('product_name', 'like', '%' . $key . '%')
                ->paginate(16);
        }

        return view('user.pages.search', compact('product', 'key', 'order', 'status'));
    }

    public function getNews(){
        $news =  DB::table('news')->orderBy('updated_at', 'desc')->take(3)->get();
        // dd($news);
        return view('user.pages.news.news', compact('news'));
    }

    public function getNewsDetail($id){
        $newsDetail = news::where('id',$id)->get();
        return view('user.pages.news.news_detail',compact('newsDetail'));
    }

    //============----------------

}
