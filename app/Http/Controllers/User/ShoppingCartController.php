<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\user;
use App\category;
use App\images;

use Cart;

class ShoppingCartController extends Controller
{


    public function themsp(Request $req,$id){
        
        $product                =   products::find($id);
        $cartinfo               =   [
                'id' => $id,
                'name' => $product->product_name,
                'price' => $product->promotion !== 0 ? $product->promotion : $product->price,
                'qty' => '1',
                'options' => [
                    'image' => $product->image,
                    'promotion' => $product->promotion,
                    '_price' => $product->price,
                    ]
        ];
        Cart::add($cartinfo);
        $data=[ 
            'count'=> Cart::count(),
        ];
        return $data;
    }

    public function updateCart($id, $value){
        Cart::update($id, ['qty' => $value]);
        
        $content = Cart::get($id);
        $total_price = $content->price * (int)$value;
        $data = [
            'total_price' => $total_price,
            'total' => Cart::subtotal()
        ];
        return $data;
    }

    public function cart(){
        return view('user.pages.cart');
    }

    public function deleteCart($id){
        Cart::remove($id);
        return 1;
    }


    public function successPay(Request $request){
        
        $content = Cart::content();
        if($content->count() == 0){
            return 0;
        }
        $name = $request->full_name;
        $address = $request->address;
        $phone = $request->phone;
        $email = $request->email;
        
       
        $data['name'] = $name;
        $data['phone'] = $phone;
        $data['email'] = $email;
        $data['address'] = $address;
        // dd( $request->full_name);
        
        $customer = customer::create($data);
    //     return $customer->id;
    // // //    dd($customer);
        $data['customer_id'] = $customer->id;
        $data['total'] = (int)str_replace(',','', explode('.', Cart::subtotal())[0]);

        $bill = bill::create($data);
        foreach($content as $row){
            $bill_detail = new bill_detail();
            $bill_detail->bill_id = $bill->id;
            $bill_detail->pro_id = $row->id;
            $bill_detail->quantity = $row->qty;
            $bill_detail->price = $row->price;
            $bill_detail->total = $row->price * $row->qty;
            $bill_detail->save();
        }
        Cart::destroy();
        return 1;
    }
    
}
