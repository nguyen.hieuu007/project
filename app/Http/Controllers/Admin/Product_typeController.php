<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\user;
use App\category;
use App\images;

class Product_typeController extends Controller
{
    //
    public function getIndex()
    {
        $product_typeList= product_type::paginate(5);
        return view('admin.pages.product_type.list',compact('product_typeList'));
    }
    public function getAdd()
    {
        $category = category::all();
        return view('admin.pages.product_type.add',compact('category'));
    }
    public function postAdd(Request $request)
    {
        $this->validate($request,
        [
            'product_type_child' => 'required|unique:product_type,name|min:3|max:100'
        ],
        [
            'product_type_child.required' => 'Bạn chưa nhập tên thể loại',
            'product_type_child.unique' => 'Tên thể loại đã bị trùng',
            'product_type_child.min' => 'Tên phải có từ 3 tới 100 ký tự',
            'product_type_child.max' => 'Tên phải có từ 3 tới 100 ký tự',
        ]);
        $product_typeAdd = new product_type;
        $product_typeAdd->name = $request->product_type_child ;
        $product_typeAdd->slug = str_slug( $request->product_type_child, '-');
        $product_typeAdd->type = $request->type;
        $product_typeAdd->save();
        return redirect()->route('product_type')->with('thongbao','Thêm thành công');
    }
    // ----------------------
    public function getEdit($id)
    {
        $product_typeEdit= product_type::find($id);
        return view('admin.pages.product_type.edit', compact('product_typeEdit'));
    }
    public function postEdit(Request $request,$id)
    {
        $product_typeEdit = product_type::find($id);
        $this->validate($request,
        [
            'product_type_child' => 'required|unique:product_type,name|min:3|max:100'
        ],
        [
            'product_type_child.required' => 'Bạn chưa nhập tên thể loại',
            'product_type_child.unique' => 'Tên thể loại đã bị trùng',
            'product_type_child.min' => 'Tên phải có từ 3 tới 100 ký tự',
            'product_type_child.max' => 'Tên phải có từ 3 tới 100 ký tự',
        ]);
        $product_typeEdit ->name = $request->product_type_child;
        $product_typeEdit->slug = str_slug($request->product_type_child, '-');
        $product_typeEdit->save();
        return redirect()->route('product_type',$product_typeEdit->id)->with('thongbao','Sửa thành công');
    }
    public function delete($id)
    {
        $product_typeDelete = product_type::find($id);
        $product_typeDelete->delete();
        return redirect()->route('product_type')->with('thongbao','Bạn đã xóa thành công!');
    }
}
