<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\users;
use App\category;
use App\images;

class UserController extends Controller
{
    //
    public function getList()
    {
        $userList = users::simplePaginate(5);
        return view('admin.pages.user.list', compact('userList'));
    }
    public function getAdd()
    {
        return view('admin.pages.user.add');
    }
    public function postAdd(Request $request)
    {
        $this->validate($request,
        [
            'user_name' => 'required|unique:user,name|min:3|max:100'
        ],
        [
            'user_name.required' => 'Bạn chưa nhập tên thể loại',
            'user_name.unique' => 'Tên thể loại đã bị trùng',
            'user_name.min' => 'Tên phải có từ 3 tới 100 ký tự',
            'user_name.max' => 'Tên phải có từ 3 tới 100 ký tự',
        ]);
        $userAdd = new users;
        $userAdd->name = $request->user_name ;
        $userAdd->email = $request->user_email;
        $userAdd->password = md5($request->user_pass);
        $userAdd->save();
        return redirect()->route('listUser',$userAdd->id)->with('thongbao','Thêm thành công');
    }
    // ----------------------
    public function getEdit($id)
    {
        $userEdit= users::find($id);
        return view('admin.pages.user.edit', compact('userEdit'));
    }
    public function postEdit(Request $request,$id)
    {
        $userEdit = users::find($id);
        $this->validate($request,
        [
            'user_name' => 'required|min:3|max:100'
        ],
        [
            'user_name.required' => 'Bạn chưa nhập tên người dùng',
            'user_name.min' => 'Tên phải có từ 3 tới 100 ký tự',
            'user_name.max' => 'Tên phải có từ 3 tới 100 ký tự',
        ]);
        $userEdit ->name = $request->user_name;
        $userEdit->email = $request->user_email;
        $userEdit->password = $request->user_pass;
        $userEdit->save();
        return redirect()->route('listUser',$userEdit->id)->with('thongbao','Sửa thành công');
    }
    public function getDelete($id)
    {
        $userDelete = users::find($id);
        $userDelete->delete();
        return redirect()->route('listUser')->with('thongbao','Bạn đã xóa thành công!');
    }

}
