<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\users;
use App\category;
use App\images;
class DashBoardController extends Controller
{
    //
    public function index(){
        $product = products::all()->count();
        $bill_check = bill::where('status', 'Đã duyệt')->count();
        $bill = bill::where('status', 'Chưa duyệt')->count();
        $user = users::all()->count();
        $customer = customer::all()->count();
        return view('admin/pages/dashboard',compact('product', 'user', 'bill_check', 'bill','customer'));
    }
}
