<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\user;
use App\category;
use App\images;

class CustomerController extends Controller
{
    //
    public function getList()
    {
        $customerList = customer::paginate(5);
        return view('admin.pages.customer.list', compact('customerList'));
    }


    public function customerSearch(Request $request){
        $key = $request->key;
        $customerList = customer::where('phone', 'like', '%' . $key . '%')->orwhere('name', 'like', '%' . $key . '%')
            ->paginate(10);
        // $customerList = customer::where('name', 'like', '%' . $key . '%')
        //     ->paginate(10);
        return view('admin.pages.customer.list', compact('customerList'));
    }


    public function getAdd()
    {
        return view('admin.pages.customer.add');
    }
    public function postAdd(Request $request)
    {
        $this->validate($request,
        [
            'customer_name' => 'required,name|min:3|max:255'
        ],
        [
            'customer_name.required' => 'Bạn chưa nhập tên thể loại',
            'customer_name.min' => 'Tên phải có từ 3 tới 255 ký tự',
            'customer_name.max' => 'Tên phải có từ 3 tới 255 ký tự',
        ]);
        $customerAdd = new customer;
        $customerAdd->name = $request->customer_name ;
        $customerAdd->phone = $request->customer_phone ;
        $customerAdd->email = $request->customer_email;
        $customerAdd->address = $request->customer_address;
        $customerAdd->save();
        return redirect()->route('listCustomer',$customerAdd->id)->with('thongbao','Thêm thành công');
    }
    // ----------------------
    public function getEdit($id)
    {
        $customerEdit= customer::find($id);
        return view('admin.pages.customer.edit', compact('customerEdit'));
    }
    public function postEdit(Request $request,$id)
    {
        $customerEdit = customer::find($id);
        $this->validate($request,
        [
            'customer_name' => 'required|min:3|max:255'
        ],
        [
            'customer_name.required' => 'Bạn chưa nhập tên người dùng',
            'customer_name.min' => 'Tên phải có từ 3 tới 255 ký tự',
            'customer_name.max' => 'Tên phải có từ 3 tới 255 ký tự',
        ]);
        $customerEdit ->name = $request->customer_name;
        $customerEdit ->phone = $request->customer_phone;
        $customerEdit->email = $request->customer_email;
        $customerEdit->address = $request->customer_address;
        $customerEdit->save();
        return redirect()->route('listCustomer',$customerEdit->id)->with('thongbao','Sửa thành công');
    }
    public function getDelete($id)
    {
        $customerBill = bill::where('customer_id', $id)->get();
        if(isset($customerBill)){
            return redirect()->route('listCustomer')->with('loi','Không thể xóa khi khách hàng đang còn hóa đơn!');
        }else{
            $customerDelete = customer::find($id);
            $customerDelete->delete();
            return redirect()->route('listCustomer')->with('thongbao','Bạn đã xóa thành công!');
        }
        
    }
}
