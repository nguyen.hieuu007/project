<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\user;
use App\category;
use App\images;

class CategoryController extends Controller
{
    //
    public function getIndex()
    {
        $categoryList= category::simplePaginate(5);
        return view('admin.pages.category.list',compact('categoryList'));
    }
    public function getAdd()
    {
        return view('admin.pages.category.add');
    }
    public function postAdd(Request $request)
    {
        $this->validate($request,
        [
            'category_child' => 'required|unique:category,name|min:3|max:100'
        ],
        [
            'category_child.required' => 'Bạn chưa nhập tên thể loại',
            'category_child.unique' => 'Tên thể loại đã bị trùng',
            'category_child.min' => 'Tên phải có từ 3 tới 100 ký tự',
            'category_child.max' => 'Tên phải có từ 3 tới 100 ký tự',
        ]);
        $categoryAdd = new category;
        $categoryAdd->name = $request->category_child ;
        $categoryAdd->slug = str_slug( $request->category_child, '-');
        $categoryAdd->save();
        return redirect()->route('postAddCategory',$categoryAdd->id)->with('thongbao','Thêm thành công');
    }
    // ----------------------
    public function getEdit($id)
    {
        $categoryEdit= category::find($id);
        return view('admin.pages.category.edit', compact('categoryEdit'));
    }
    public function postEdit(Request $request,$id)
    {
        $categoryEdit = category::find($id);
        $this->validate($request,
        [
            'category_child' => 'required|unique:category,name|min:3|max:100'
        ],
        [
            'category_child.required' => 'Bạn chưa nhập tên thể loại',
            'category_child.unique' => 'Tên thể loại đã bị trùng',
            'category_child.min' => 'Tên phải có từ 3 tới 100 ký tự',
            'category_child.max' => 'Tên phải có từ 3 tới 100 ký tự',
        ]);
        $categoryEdit ->name = $request->category_child;
        $categoryEdit->slug = str_slug($request->category_child, '-');
        $categoryEdit->save();
        return redirect()->route('category',$categoryEdit->id)->with('thongbao','Sửa thành công');
    }
    public function delete($id)
    {
        $categoryDelete = category::find($id);
        $categoryDelete->delete();
        return redirect()->route('category')->with('thongbao','Bạn đã xóa thành công!');
    }
}
