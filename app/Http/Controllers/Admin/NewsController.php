<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\user;
use App\category;
use App\images;

class NewsController extends Controller
{
    //
    public function getList()
    {
        $newsList= news::simplePaginate(5);
        return view('admin.pages.news.list',compact('newsList'));
    }
    public function getAdd()
    {
        return view('admin.pages.news.add');
    }
    public function postAdd(Request $request)
    {
        
        $newsAdd = new news;
        $newsAdd->article = $request->news_child ;
        $newsAdd->slug = str_slug( $request->news_child, '-');

// dd($request->file('image'));
        if ($request->hasFile('image')) {
            $file=$request->file('image');
            $duoi=strtolower($request->file('image')->getClientOriginalExtension());
            // dd($duoi);
            if($duoi !== 'jpg' && $duoi !== 'png' &&  $duoi !=='jpeg'){
                return redirect()->route('c',$id)->with('error','Upload file không thành công');
            }
            $size = $_FILES['image']['size'];
            if ($size > 2*2048*2048 ){
                return redirect()->route('getAddNews',$id)->with('error','Dung lượng file vượt quá 2mb');
            }
            $name=$file->getClientOriginalName();
            $url = public_path().'/upload/image/news/'.$name;
            if(file_exists($url)){
                unlink('upload/image/news/'.$name);
            }
            $hinh=str_random(4).'_'.$name;
            while(file_exists('upload/image/news/'.$hinh)){
                $hinh=str_random(4).'_'.$name;
            }
            $file->move('upload/image/news/',$hinh);
            $newsAdd->image = $hinh;
        } else {
            $request->image='';
        }

        $newsAdd->content = $request->news_content;

        $newsAdd->save();
        return redirect()->route('listNews',$newsAdd->id)->with('thongbao','Thêm thành công');
    }
    // ----------------------
    public function getEdit($id)
    {
        $newsEdit= news::find($id);
        return view('admin.pages.news.edit', compact('newsEdit'));
    }
    public function postEdit(Request $request,$id)
    {
        $newsEdit = news::find($id);
        

        $newsEdit ->article = $request->news_child;
        $newsEdit->slug = str_slug($request->news_child, '-');

        if ($request->hasFile('image')) {
            $file=$request->file('image');
            $duoi=strtolower($request->file('image')->getClientOriginalExtension());
            // dd($duoi);
            if($duoi !== 'jpg' && $duoi !== 'png' &&  $duoi !=='jpeg'){
                return redirect()->route('getEditNews',$id)->with('error','Upload file không thành công');
            }
            $size = $_FILES['image']['size'];
            if ($size > 2*2048*2048 ){
                return redirect()->route('getEditNews',$id)->with('error','Dung lượng file vượt quá 2mb');
            }
            $name=$file->getClientOriginalName();
            $url = public_path().'/upload/image/news/'.$name;
            if(file_exists($url)){
                unlink('upload/image/news/'.$name);
            }
            $hinh=str_random(4).'_'.$name;
            while(file_exists('upload/image/news/'.$hinh)){
                $hinh=str_random(4).'_'.$name;
            }
            $file->move('upload/image/news/',$hinh);
            $newsEdit->image = $hinh;
        } else {
            $request->image='';
        }



        $newsEdit ->content = $request->news_content;
        $newsEdit->save();
        return redirect()->route('listNews',$newsEdit->id)->with('thongbao','Sửa thành công');
    }
    public function getDelete($id)
    {
        $newsDelete = news::find($id);
        $newsDelete->delete();
        return redirect()->route('listNews')->with('thongbao','Bạn đã xóa thành công!');
    }
}

