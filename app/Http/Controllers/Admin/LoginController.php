<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;

use App\users;

class LoginController extends Controller
{
    //
    public function login(){
        return view('admin.login');
    }

    public function postLogin(Request $request){
        $this->validate($request, [
            'email' => 'required|min:3|max:100',
            'password' => 'required|min:3|max:100'

        ],[
            'email.required' => 'Tài khoản không được để chống',
            'email.min' => 'Tài khoản từ 3 đến 100 ký tự',
            'email.max' => 'Tài khoản từ 3 đến 100 ký tự',
            'password.required' => 'Mật khẩu không được để chống',
            'password.min' => 'Tài khoản từ 3 đến 100 ký tự',
            'password.max' => 'Tài khoản từ 3 đến 100 ký tự',
        ]);
        // $salt = 'killsky';
        $email = $request->email;
        $password = md5($request->password);
        $user = users::where('email',$email)->where('password',$password)->first();
        if(isset($user)){
            $aa=$user->name;
            Session::put('user', $user);
            return redirect()->route('dashboard');
        }else{
            return redirect()->route('adminLogin')->with('error', 'Tài khoản hoặc mật khẩu chưa đúng');
        }
    }

    public function logout(){
        Session::forget('user');
        return redirect()->route('adminLogin');
    }
}
