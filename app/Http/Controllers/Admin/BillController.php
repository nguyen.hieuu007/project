<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\user;
use App\category;
use App\images;

class BillController extends Controller
{
    public function getBillCheck(){
        $bill = bill::where('status', 'Đã duyệt')->orderBy('updated_at','desc')->simplePaginate(10);
        return view('admin.pages.bill.list_check', compact('bill'));
    }

    public function getBillTemp(){
        $bill = bill::where('status', 'Chưa duyệt')->orderBy('updated_at','asc')->simplePaginate(10);
        return view('admin.pages.bill.list_temp', compact('bill'));
    }

    public function passBill($id){
        $bill = bill::find($id);
        $bill->status = 'Đã duyệt';
        $bill->save();
        return redirect()->route('billTemp')->with('success', 'Duyệt sản phẩm thành công');
    }

    public function getDetailsBill($id){
        $bill_details = bill_detail::where('bill_id', $id)->get();
        $bill_date = bill::where('id',$id)->get();
        return view('admin.pages.bill.bill_details', compact('bill_details','bill_date'));
    }

    public function updateStatus(Request $request){
        $id = $request->id;
        $status = $request->status;
        $bill = bill::where('id', $id)->first();
        $bill->status_transfer = $status;
        $bill->save();
        return 1;
    }

    public function getDelete($id){
        $bill_details = bill_detail::where('bill_id', $id)->delete();
        $bill = bill::find($id);
        $bill->delete();
        return 1;
    }

}
