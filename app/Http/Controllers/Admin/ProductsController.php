<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product_type;
use App\products;
use App\bill_detail;
use App\bill;
use App\brand;
use App\customer;
use App\news;
use App\user;
use App\category;
use App\images;
use App\product_image;

class ProductsController extends Controller
{
    //
    public function getIndex()
    {
        $productsList= products::paginate(5);
        return view('admin.pages.products.list',compact('productsList'));
    }

    public function productsSearch(Request $request){
        $key = $request->key;
        $productsList = products::where('product_name', 'like', '%' . $key . '%')
            ->paginate(10);
        return view('admin.pages.products.list',compact('productsList'));
    }


    public function getAdd()
    {
        $products= products::all();
        $category= category::all();
        $brand = brand::all();
        return view('admin.pages.products.add', compact('products','category','brand'));
    }

    public function loadCategory($id){
        $product_type = product_type::where('type',$id)->get();
        $html = '';
        foreach($product_type as $row){
            $html = $html."<option value='".$row->id."'>".$row->name."</option>";
        }
        return $html;
    }

    public function postAdd(Request $request)
    {
        // dd($request->image_details);
        $this->validate($request,
        [
            'products_name' => 'required|unique:products,product_name|min:3|max:255',
            'products_brand'=>'required',
            'products_infor'=>'required',
            'products_price' => 'numeric|min:0',
            'products_promotion' => 'numeric|min:0',
        ],
        [
            'products_name.required' => '*Bạn chưa nhập tên sản phẩm',
            'products_name.unique' => '*Tên sản phẩm đã bị trùng',
            'products_name.min' => '*Tên phải có từ 3 tới 255 ký tự',
            'products_name.max' => '*Tên phải có từ 3 tới 255 ký tự',
            'products_brand'=>'*Bạn chưa chọn nhãn hiệu',
            'products_infor.required' => '*Bạn điền thông tin sản phẩm',
            'products_price.numeric' => '*Giá phải là chữ số',
            'products_promotion.numeric' => '*Khuyến mại phải là chữ số',
        ]);
        $productsAdd = new products;
        $productsAdd->product_name = $request->products_name ;
        $productsAdd->slug = str_slug( $request->products_name, '-');
        $productsAdd->brand_id = $request->products_brand;
        $productsAdd->protype_id = $request->products_protype;
        $productsAdd->price = $request->products_price;
        $productsAdd->promotion = $request->products_promotion;
        
        if ($request->products_promotion > $request->products_price) {
            return redirect()->route('getAddProducts')->with('error', 'Giá khuyến mãi phải nhỏ hơn giá gốc');
        }

        if ($request->hasFile('image')) {
            $file=$request->file('image');
            $duoi=$file->getClientOriginalExtension();
            if($duoi !== 'jpg' && $duoi !== 'png' &&  $duoi !=='jpeg'){
                return redirect()->route('getAddProducts')->with('error','Upload file không thành công');
            }
            $size = $_FILES['image']['size'];
            if ($size > 2*2048*2048 ){
                return redirect()->route('getAddProducts')->with('error','Dung lượng file vượt quá 2mb');
            }
            $name=$file->getClientOriginalName();
            $hinh=str_random(4).'_'.$name;
            while(file_exists('upload/image/products/'.$hinh)){
                $hinh=str_random(4).'_'.$name;
            }
            $file->move('upload/image/products/',$hinh);
            $productsAdd->image = $hinh;
        } else {
            $request->image='';
        }

        $productsAdd->infor = $request->products_infor;
        $productsAdd->save();
// ---------------
   
        
        $file_img=$request->file('image_details');
        
        foreach($file_img as $row){
            
            $product_image = new product_image;

             $product_image->product_id =$productsAdd->id;

            $duoi=strtolower($row->getClientOriginalExtension());
            
            
            if($duoi !== 'jpg' && $duoi !== 'png' &&  $duoi !=='jpeg'){
                return redirect()->route('getAddProducts',$id)->with('error','Upload file không thành công');
            }
            $size = $_FILES['image']['size'];
            if ($size > 2*2048*2048 ){
                return redirect()->route('getAddProducts',$id)->with('error','Dung lượng file vượt quá 2mb');
            }
            $name=$row->getClientOriginalName();
            
            $url = public_path().'/upload/image/products/'.$name;
            // echo '<pre>';
            // var_dump($url);
            // echo '<pre>';
            if(file_exists($url)){
                unlink('upload/image/products/'.$name);
            }
            $hinh=str_random(4).'_'.$name;
            while(file_exists('upload/image/products/'.$hinh))
            {
                $hinh=str_random(4).'_'.$name;
            }
            $row->move('upload/image/products/',$hinh);
            $product_image->url_images = $hinh;

            $product_image->save();
        }
        // die();
        return redirect()->route('products')->with('thongbao','Thêm thành công');


    }



    public function getEdit($id)
    {   
        $productsEdit= products::find($id);
        $category= category::all();
        $brand = brand::all();
        $product_imageEdit= product_image::Where('product_id',$id)->get();
        // dd($product_imageEdit);
        return view('admin.pages.products.edit', compact('productsEdit','category','brand','product_imageEdit'));
    }
    
    public function postEdit(Request $request,$id)
    {
  
        $productsEdit  = products::find($id);
        $product_imageEdit= product_image::where('product_id',$id)->get();
        $this->validate($request,
        [
            'products_name' => 'required|unique:products,product_name,'.$id.',id|min:3|max:255',
            'products_brand'=>'required',
            'products_infor'=>'required',
            'products_price' => 'numeric|min:0',
            'products_promotion' => 'numeric|min:0',
        ],
        [
            'products_name.required' => '*Bạn chưa nhập tên sản phẩm',
            'products_name.unique' => '*Tên sản phẩm đã bị trùng',
            'products_name.min' => '*Tên phải có từ 3 tới 255 ký tự',
            'products_name.max' => '*Tên phải có từ 3 tới 255 ký tự',
            'products_brand'=>'*Bạn chưa chọn nhãn hiệu',
            'products_infor.required' => '*Bạn điền thông tin sản phẩm',
            'products_price.numeric' => '*Giá phải là chữ số',
            'products_promotion.numeric' => '*Khuyến mại phải là chữ số',
        ]);
        $productsEdit->product_name = $request->products_name ;
        $productsEdit->slug = str_slug( $request->products_name, '-');
        $productsEdit->brand_id = $request->products_brand;
        $productsEdit->protype_id = $request->products_protype;
        $productsEdit->price = $request->products_price;
        $productsEdit->promotion = $request->products_promotion;
        if ($request->products_promotion > $request->products_price) {
            return redirect()->route('getEditProducts')->with('error', 'Giá khuyến mãi phải nhỏ hơn giá gốc');
        }

        if ($request->hasFile('image')) {
            $file=$request->file('image');
            $duoi=strtolower($request->file('image')->getClientOriginalExtension());
            // dd($duoi);
            if($duoi !== 'jpg' && $duoi !== 'png' &&  $duoi !=='jpeg'){
                return redirect()->route('getEditProducts',$id)->with('error','Upload file không thành công');
            }
            $size = $_FILES['image']['size'];
            if ($size > 2*2048*2048 ){
                return redirect()->route('getEditProducts',$id)->with('error','Dung lượng file vượt quá 2mb');
            }
            $name=$file->getClientOriginalName();
            $url = public_path().'/upload/image/products/'.$productsEdit->image;
            if(file_exists($url)){
                unlink('upload/image/products/'.$productsEdit->image);
            }
            $hinh=str_random(4).'_'.$name;
            while(file_exists('upload/image/products/'.$hinh)){
                $hinh=str_random(4).'_'.$name;
            }
            $file->move('upload/image/products/',$hinh);
            $productsEdit->image = $hinh;
        } else {
            $request->image='';
        }
            // ----trên này là phnaf ảnh chính
        $product_image_details = product_image::where('product_id',$id)->get();
        //dd($product_image_details);
        foreach($product_image_details as $key => $row){
            if($request->has('img_'.$row->id)){
                $file=$request->file('img_'.$row->id);

                $duoi=strtolower($file->getClientOriginalExtension());
            //     echo '<pre>';
            // var_dump($duoi);
            // echo '<pre>';
                if($duoi !== 'jpg' && $duoi !== 'png' &&  $duoi !=='jpeg'){
                    return redirect()->route('getEditProducts',$id)->with('error','Upload file không thành công');
                }
                $size = $_FILES['image']['size'];
                if ($size > 2*2048*2048 ){
                    return redirect()->route('getEditProducts',$id)->with('error','Dung lượng file vượt quá 2mb');
                }
                $name=$file->getClientOriginalName();

                $url = public_path().'/upload/image/products/'.$name;
                if(file_exists($url)){
                    unlink('upload/image/products/'.$name);
                }
                $hinh=str_random(4).'_'.$name;
                while(file_exists('upload/image/products/'.$hinh)){
                    $hinh=str_random(4).'_'.$name;
                }
                $file->move('upload/image/products/',$hinh);
                $row->url_images = $hinh;
                $row->save();
                
            }
            
        }
        // die();
       

        $productsEdit->infor = $request->products_infor;
        $productsEdit->save();
        return redirect()->route('products',$productsEdit->id)->with('thongbao','Sửa thành công');
    }

    // public function delete($id)
    // {
    //     $product_image = product_image::where('product_id',$id)->get();
    //     $product_image->delete();
    //     $productsDelete = products::find($id);
        
        
    //     if(isset($productsDelete)){
            
    //         $productsDelete->delete();
            
    //     }
        
    //     return redirect()->route('products')->with('thongbao','Bạn đã xóa thành công!');
    // }
    public function getDeleteImage($id){
        $image = product_image::find($id);

        
            $image->delete();

        return 0;

    }

    public function delete($id){
        $product = products::find($id);
        foreach ($product->product_image as $row){
            $this->getDeleteImage($row->id);
        }
        $product->delete();
        return redirect()->route('products')->with('thongbao','Bạn đã xóa thành công!');
    }

    

}




// ----------------------------
        
        // if($request->file('image')) {

        //     /*
        //         Kiểm tra định dạng, size
        //     */
        //     if($_FILES['image']['error'] != 0){
        //         return redirect()->route('getAddProducts')->with('error','Upload file không thành công');
        //     }
        //     $extend = strtolower($request->file('image')->getClientOriginalExtension());
        //     $name = beautiName($_FILES['image']['name']).'.'.$extend ;

        //     if ($extend == 'jpg' || $extend == 'png' || $extend == 'jpeg' || $extend == 'gif'){
        //         $size = $_FILES['image']['size'];
        //         if ($size > 2*2048*2048 ){
        //             return redirect()->route('getAddProducts')->with('error','Dung lượng file vượt quá 2mb');
        //         }
        //         $path = date('H-i-s-Y-m-d').'-'.$name;
        //         $request->file('image')->move(public_path().'/upload/image/products/',$path);
        //     } else {
        //         return redirect()->route('getAddProducts')->with('error','Sai định dạng file');
        //     }
        // }
        // ----------------