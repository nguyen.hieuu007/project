<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    protected $table = "products";

    public function product_type(){
        return $this->belongsTo('App\product_type','protype_id','id');
    }
    public function brand(){
        return $this->belongsTo('App\brand','brand_id','id');
    }
    public function bill_detail(){
        return $this->hasMany('App\bill_detail','bill_id','id');
    }
    public function product_image(){
        return $this->hasMany('App\product_image','product_id','id');
    }
}
