<?php
    function menuMulti($data, $parent_id = 0, $str="----", $select_id){
        foreach($data as $key => $val){
            $id = $val['id'];
            $name = $val['name'];
            $slug = $val['slug'];
            if ($val['parent_id'] == $parent_id){
                if($select_id != '' && $select_id == $id){
                    echo '<option selected="" value="'.$id.'">'.$str.$name.'</option>';
                }else{
                    echo '<option value="'.$id.'">'.$str.$name.'</option>';
                }

                unset($data[$key]);
                menuMulti($data, $id, $str."|----", $select_id);
            }

        }
    }

    function beautiName($name){
        $arr = ['jpg', 'jpeg', 'gif', 'png'];
        $name = str_replace($arr, '', $name);
        return str_slug($name);
    }

function menuMultiView($data){
    foreach($data as $key => $val){
        $id = $val['id'];
        $name = $val['name'];
        $slug = $val['slug'];
        if ($val['parent_id'] == 0){
            echo '<li class="no-have-mega">
                        <a href="'.route('listProductByCategory', $slug).'" title="'.$name.'"><img class="iconmenu" src="http://via.placeholder.com/402x300" style="margin-right: 10px;">'.$name.'</a>';
            if (countItem($data, $id) == 1){
                echo        '<i class="click_open_sub_megamenu">+</i>';
                echo        '<ul class="devvn_sub_menu">';
                menuViewChild($data, $id);
                echo '</ul>';
            }

            echo '</li>';

        }

    }
}

function menuViewChild($data, $id){
        $d = 0;
        foreach ($data as $key => $row){

            if($row['parent_id'] == $id){
                $d = $d + 1;
                echo '<li>
                            <a href="'.route('listProductByCategory', $row['slug']).'" title="'.$row['name'].'">'.$row['name'].'
                            </a>
                        </li>';
            }
        }
        if ($d !== 0) {
            return 1;
        }else{
            return 0;
        }
}

function countItem($data, $id){
    $d = 0;
    foreach ($data as $key => $row){

        if($row['parent_id'] == $id){
            $d = $d + 1;
        }
    }

    if ($d !== 0) {
        return 1;
    }else{
        return 0;
    }
}

function menuMultiView1($data){
    foreach($data as $key => $val){
        $id = $val['id'];
        $name = $val['name'];
        $slug = $val['slug'];
        if ($val['parent_id'] == 0){
            if (countItem($data, $id) == 0){
                echo '<li><a level="1" title="" href="'.route('listProductByCategory', $slug).'">'.$name.'</a></li>';
            }else{
                echo '<li class="dropdown">';
                echo            '<a level="1" title="'.$name.'" href="'.route('listProductByCategory', $slug).'">'.$name.'</a>';
                echo            '<ul class="dropdown-menu" style="    left: 228px;top: 0px;">';
                                
                                menuViewChild1($data, $id);
                echo            '</ul>';
                echo        '</li>';
            }

        }

    }
}

function menuViewChild1($data, $id){
    $d = 0;
    foreach ($data as $key => $row){

        if($row['parent_id'] == $id){
            $d = $d + 1;
            echo '<li><a title="'.$row['name'].'" href="'.route('listProductByCategory', $row['slug']).'" target="" class="top_link">'.$row['name'].'</a>
                    </li>';
        }
    }
    if ($d !== 0) {
        return 1;
    }else{
        return 0;
    }
}

function menuMultiViewResponsive($data){
    foreach($data as $key => $val){
        $id = $val['id'];
        $name = $val['name'];
        $slug = $val['slug'];
        if ($val['parent_id'] == 0){
            if (countItem($data, $id) == 0){
                echo '<a target="_self" class="list-group-item-stmenu" href="'.route('listProductByCategory', $slug).'">'.$name.'</a>';
            }else{
                echo '<a href="#'.$id.'" data-toggle="collapse" target="_self" class="list-group-item-stmenu">'.$name.'</a>';
                echo '<a href="#'.$id.'" data-toggle="collapse" class="arrow-sub"><i class="fa fa-angle-down"></i></a>';
                echo '<div class="collapse" id="'.$id.'">';
                menuViewChildResponsive($data, $id);
                echo '</div>';
            }

        }

    }
}

function menuViewChildResponsive($data, $id){
    $d = 0;
    foreach ($data as $key => $row){

        if($row['parent_id'] == $id){
            $d = $d + 1;
            echo '<a title="'.$row['name'].'" href="'.route('listProductByCategory', $row['slug']).'" target="_self" class="list-group-item-stmenu sub">'.$row['name'].'</a>';
        }
    }
    if ($d !== 0) {
        return 1;
    }else{
        return 0;
    }
}

?>