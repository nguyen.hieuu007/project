<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_type extends Model
{
    //
    protected $table = "product_type";

    public function products(){
        return $this->hasMany('App\products','protype_id','id');
    }
    public function category(){
        return $this->belongsTo('App\category','type','id');
    }
}
