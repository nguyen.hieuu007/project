<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = "category";
    public function product_type(){
        return $this->hasMany('App\product_type','type','id');
    }
    // public function products(){
    //     return $this->hasMany('App\products','')
    // }
}
