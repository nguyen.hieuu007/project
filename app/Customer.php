<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    //
    protected $table = "customer";


    protected $primaryKey = 'id';
    protected $fillable = ['id','name','phone','email','address'];

    public function bill(){
        return $this->hasMany('App\bill','customer_id','id');
    }
}
