<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    //
    protected $table = "bill";

    protected $primaryKey = 'id';
    protected $fillable = ['id','customer_id','total','status','status_trasfer'];




    public function customer(){
        return $this->belongsTo('App\customer','customer_id','id');
    }
    public function bill_detail(){
        return $this->hasMany('App\bill_detail','bill_id','id');
    }
}
