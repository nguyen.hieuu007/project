<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Cart;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //đây là kiểu như là nó truyền biến sang bên cart sang bên header của anh ý
        view()->composer('user.header',function($view){
            $cart=Cart::content();
            $view->with(['cart' => $cart]);
        });
        view()->composer('user.master',function($view){
            $cart=Cart::content();
            $view->with(['cart' => $cart]);
        });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
