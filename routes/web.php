<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//// lẹt gâu:
// -------------------user-------------------
Route::group(['namespace'=>'User'], function(){
    // index
    Route::get('/',['as'=>'index', 'uses'=>'IndexController@getIndex']);

    // products
    Route::get('san-pham/{id}',['as'=>'products_user','uses'=>'IndexController@getProducts']);
    Route::get('danh-sach-sp/{id}',['as'=>'listProducts_user', 'uses'=>'IndexController@getListProducts']);
    Route::get('chi-tiet-san-pham/{id}',['as'=>'detailProducts', 'uses'=>'IndexController@getDetailProducts']);


    // // khuyenmai

    Route::get('khuyen-mai',['as'=>'promotion','uses'=>'IndexController@getPromotion']);

    // tintuc

    Route::get('tin-tuc',['as'=>'news','uses'=>'IndexController@getNews']);
    Route::get('xem-tin-tuc/{id}',['as'=>'NewsDetail','uses'=>'IndexController@getNewsDetail']);

    //cart 
    Route::get('addcart/{id}',['as' => 'addcart','uses' => 'ShoppingCartController@themsp']);
    Route::get('them-hang/{id}/{value}',['as' => 'addToCart', 'uses' => 'ShoppingCartController@addToCart']);
    Route::get('gio-hang',['as' => 'cart', 'uses' => 'ShoppingCartController@cart']);
    Route::get('xem-san-pham/{id}',['as' => 'getDetailProducts', 'uses' => 'IndexController@getDetailProducts']);
    Route::get('thanh-toan',['as' => 'pay', 'uses' => 'ShoppingCartController@pay']);
    Route::get('update-cart/{id}/{value}',['as' => 'updateCart', 'uses' => 'ShoppingCartController@updateCart']);
    Route::get('delete-cart/{id}', ['as' => 'deleteCart', 'uses' => 'ShoppingCartController@deleteCart']);
    Route::get('thanh-toan-san-pham', ['as' => 'successPay', 'uses' => 'ShoppingCartController@successPay']);

    Route::get('tim-kiem', ['as' => 'search', 'uses' => 'IndexController@search']);

});




// -------------------admin--------------


Route::get('admin/login', ['as' => 'adminLogin', 'uses' => 'Admin\LoginController@login']);
Route::post('admin/login', ['as' => 'adminPostLogin', 'uses' => 'Admin\LoginController@postLogin']);
// , 'middleware' => 'adminLogin'
Route::group(['prefix' => 'admin', 'namespace' => 'Admin','middleware'=>'AdminMiddleware'], function(){
    Route::get('logout', ['as' => 'adminLogout', 'uses' => 'LoginController@logout']);
    //----------------------------------------------------------------------
    Route::get('', ['as' => 'dashboard', 'uses' => 'DashBoardController@index']);

    Route::group(['prefix' => 'danh-muc'], function(){
        Route::get('', ['as' => 'category', 'uses' => 'CategoryController@getIndex']);
        Route::get('them',['as' => 'getAddCategory', 'uses' => 'CategoryController@getAdd']);
        Route::post('them',['as' => 'postAddCategory', 'uses' => 'CategoryController@postAdd']);
        Route::get('sua/{id}',['as' => 'getEditCategory', 'uses' => 'CategoryController@getEdit']);
        Route::post('sua/{id}',['as' => 'postEditCategory', 'uses' => 'CategoryController@postEdit']);
        Route::get('xoa/{id}', ['as' => 'deleteCategory', 'uses' => 'CategoryController@delete']);
    });
    Route::group(['prefix' => 'loai-san-pham'], function(){
        Route::get('', ['as' => 'product_type', 'uses' => 'Product_typeController@getIndex']);
        Route::get('them',['as' => 'getAddProduct_type', 'uses' => 'Product_typeController@getAdd']);
        Route::post('them',['as' => 'postAddProduct_type', 'uses' => 'Product_typeController@postAdd']);
        Route::get('sua/{id}',['as' => 'getEditProduct_type', 'uses' => 'Product_typeController@getEdit']);
        Route::post('sua/{id}',['as' => 'postEditProduct_type', 'uses' => 'Product_typeController@postEdit']);
        Route::get('xoa/{id}', ['as' => 'deleteProduct_type', 'uses' => 'Product_typeController@delete']);

    });

    Route::group(['prefix' => 'san-pham'], function(){
        Route::get('', ['as' => 'products', 'uses' => 'ProductsController@getIndex']);
        Route::get('them',['as' => 'getAddProducts', 'uses' => 'ProductsController@getAdd']);
        Route::post('them',['as' => 'postAddProducts', 'uses' => 'ProductsController@postAdd']);
        Route::get('sua/{id}',['as' => 'getEditProducts', 'uses' => 'ProductsController@getEdit']);
        Route::post('sua/{id}',['as' => 'postEditProducts', 'uses' => 'ProductsController@postEdit']);
        Route::get('xoa-anh/{id}',['as' => 'getDeleteImageProducts', 'uses' => 'ProductsController@getDeleteImage']);
        Route::get('xoa/{id}', ['as' => 'deleteProducts', 'uses' => 'ProductsController@delete']);
        Route::get('load-category/{id}',['as' => 'loadCategory', 'uses' => 'ProductsController@loadCategory']);
        Route::get('products_search',['as' => 'search_product', 'uses' => 'ProductsController@productsSearch']);
    });

    Route::group(['prefix' => 'slide'], function(){
        Route::get('', ['as' => 'slide', 'uses' => 'SlideController@getIndex']);
        Route::get('them',['as' => 'getAddSlide', 'uses' => 'SlideController@getAdd']);
        Route::post('them',['as' => 'postAddSlide', 'uses' => 'SlideController@postAdd']);
        Route::get('sua/{id}',['as' => 'getEditSlide', 'uses' => 'SlideController@getEdit']);
        Route::post('sua/{id}',['as' => 'postEditSlide', 'uses' => 'SlideController@postEdit']);
        Route::get('xoa/{id}', ['as' => 'deleteSlide', 'uses' => 'SlideController@getDelete']);
    });

    Route::group(['prefix' => 'hoa-don'], function(){
        Route::get('da-duyet', ['as' => 'billCheck', 'uses' => 'BillController@getBillCheck']);
        Route::get('chi-tiet-hoa-don/{id}', ['as' => 'billDetails', 'uses' => 'BillController@getDetailsBill']);
        Route::get('chua-duyet', ['as' => 'billTemp', 'uses' => 'BillController@getBillTemp']);
        Route::get('duyet/{id}',['as' => 'billPass', 'uses' => 'BillController@passBill']);
        Route::get('cap-nhat-trang-thai', ['as' => 'updateStatus', 'uses' => 'BillController@updateStatus']);
        Route::get('sua/{id}',['as' => 'getEditBill', 'uses' => 'BillController@getEdit']);
        Route::post('sua/{id}',['as' => 'postEditBill', 'uses' => 'BillController@postEdit']);
        Route::get('xoa/{id}', ['as' => 'deleteBill', 'uses' => 'BillController@getDelete']);
    });

    Route::group(['prefix' => 'tin-tuc'], function(){
        Route::get('', ['as' => 'listNews', 'uses' => 'NewsController@getList']);
        Route::get('them', ['as' => 'getAddNews', 'uses' => 'NewsController@getAdd']);
        Route::post('them', ['as' => 'postAddNews', 'uses' => 'NewsController@postAdd']);
        Route::get('sua/{id}', ['as' => 'getEditNews', 'uses' => 'NewsController@getEdit']);
        Route::post('sua/{id}', ['as' => 'postEditNews', 'uses' => 'NewsController@postEdit']);
        Route::get('xoa/{id}', ['as' => 'getDeleteNews', 'uses' => 'NewsController@getDelete']);

    });

    Route::group(['prefix' => 'nguoi-dung'], function(){
        Route::get('', ['as' => 'listUser', 'uses' => 'UserController@getList']);
        Route::get('them', ['as' => 'getAddUser', 'uses' => 'UserController@getAdd']);
        Route::post('them', ['as' => 'postAddUser', 'uses' => 'UserController@postAdd']);
        Route::get('sua/{id}', ['as' => 'getEditUser', 'uses' => 'UserController@getEdit']);
        Route::post('sua/{id}', ['as' => 'postEditUser', 'uses' => 'UserController@postEdit']);
        Route::get('xoa/{id}', ['as' => 'getDeleteUser', 'uses' => 'UserController@getDelete']);

    });

    Route::group(['prefix' => 'khach-hang'], function(){
        Route::get('', ['as' => 'listCustomer', 'uses' => 'CustomerController@getList']);
        Route::get('them', ['as' => 'getAddCustomer', 'uses' => 'CustomerController@getAdd']);
        Route::post('them', ['as' => 'postAddCustomer', 'uses' => 'CustomerController@postAdd']);
        Route::get('sua/{id}', ['as' => 'getEditCustomer', 'uses' => 'CustomerController@getEdit']);
        Route::post('sua/{id}', ['as' => 'postEditCustomer', 'uses' => 'CustomerController@postEdit']);
        Route::get('xoa/{id}', ['as' => 'getDeleteCustomer', 'uses' => 'CustomerController@getDelete']);
        Route::get('customer-search', ['as' => 'search_cutomer', 'uses' => 'CustomerController@customerSearch']);
    });

    Route::group(['prefix' => 'lien-he'], function(){
        Route::get('', ['as' => 'listContact', 'uses' => 'ContactController@getList']);
        Route::get('delete/{id}', ['as' => 'deleteContact', 'uses' => 'ContactController@getDelete']);
    });

});
