<style>

    .send_mail {
    text-transform: uppercase;
    background: #231f20;
    color: #fff;
    font-family: Roboto-Light;
    padding: 9px 25px;
    /* margin-right: 30px; */
    /* float: left; */
    text-decoration: none;
    }
    a{
        text-decoration: none;
    }
    a.send_mail :hover{
        background:yellow;
    }
</style>
  
<!-- brand -->
    <div class="yellow-bg mt-50">
        <div class="brands">THƯƠNG HIỆU</div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/martin.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/takamine.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/taylor.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/casio.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/cordoba.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/yamaha.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/fender.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/kawai.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/epiphone.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/ibanez.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/roland.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="brand">
                        <img src="{{asset('')}}asset/user/brands/thompson.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- lienhe -->
    <div class="container my-3">
        <div  id="contact" align="center">
            <h4><b>LIÊN HỆ</b></h4>
            <p style="margin:20px">Liên hệ với chúng tôi để tìm ra giải pháp cho niềm đam mê âm nhạc của bạn.</p>
            <a href="mailto:nguyen.hieuu007@gmail.com" class="send_mail">GỬI E-MAIL</a>
            <a href="tell:0123456789"  class="send_mail">HOTLINE</a>

        </div>
    </div>
<!-- footer -->
<footer class="bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6 col-12">
                    <div class="mb-3"><b>THÔNG TIN</b></div>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="" class="nav-link">Giới thiệu</a></li>
                        <li class="nav-item"><a href="" class="nav-link">Tuyển dụng</a></li>
                        <li class="nav-item"><a href="" class="nav-link">Liên hệ / Góp ý</a></li>
                        <li class="nav-item"><a href="" class="nav-link">Đại lí</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6 col-12">
                    <div class="mb-3"><b>HỖ TRỢ KHÁCH HÀNG</b></div>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="" class="nav-link">Hướng dẫn mua hàng</a></li>
                        <li class="nav-item"><a href="" class="nav-link">Câu hỏi thường gặp</a></li>
                        <li class="nav-item"><a href="" class="nav-link">Giải quyết khiếu nại</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6 col-12">
                    <div class="mb-3"><b>CHÍNH SÁCH</b></div>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="" class="nav-link">Chính sách giao hàng</a></li>
                        <li class="nav-item"><a href="" class="nav-link">Chính sách bảo hành và đổi trả</a></li>
                        <li class="nav-item"><a href="" class="nav-link">Chính sách thanh toán</a></li>
                        <li class="nav-item"><a href="" class="nav-link">Chính sách bảo mật thông itn</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6 col-12">
                    <div class="mb-3"><b>SOCIAL</b></div>
                    <ul class="navbar-nav navbar-expand" id="social">
                        <li class="nav-item"><a href="" class="nav-link"><img src="{{asset('')}}asset/user/icons/facebook.png" alt=""></a></li>
                        <li class="nav-item"><a href="" class="nav-link"><img src="{{asset('')}}asset/user/icons/google.png" alt=""></a></li>
                        <li class="nav-item"><a href="" class="nav-link"><img src="{{asset('')}}asset/user/icons/twitter.png" alt=""></a></li>
                        <li class="nav-item"><a href="" class="nav-link"><img src="{{asset('')}}asset/user/icons/youtube.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
            <hr>
        </div>
    </footer>