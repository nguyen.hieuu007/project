@extends('user.master')

@section('content')

    
    <!-- Services -->
    <div class="container mt-50">
        <div class="row services">
            <div class="col-12 col-md-4 xs-hidden">
                <button>
                    <span class="fa fa-dollar-sign"></span>
                    Trả góp
                </button>
            </div>
            <div class="col-12 col-md-4 xs-hidden">
                <button>
                    <span class="fa fa-shipping-fast"></span>
                    Giao hàng nhanh
                </button>
            </div>
            <div class="col-12 col-md-4 col-xs-hd">
                <button>
                    <img src="{{asset('')}}asset/user/icons/protect.png" alt="">
                    Bảo hành
                </button>
            </div>
        </div>
    </div>
    <!-- Products -->
    <div class="container mt-50">
        <div class="category-title">
            <p>SẢN PHẨM NỔI BẬT</p>
        </div>
        <div class="row stand-out-products">
            @foreach ($product_hot as $row)
            <div class="col-lg-3 col-sm-6 col-12">
                <div style="border: 2px solid #c5b13e00;border-radius: 15px;padding:5px">
                    <h5 style="color: brown;text-transform: uppercase;margin-top:5px">{{$row->product_name}}</h5>
                    <a href="{{route('detailProducts',$row->id)}}"><img src="{{ asset('upload/image/products/'.$row->image) }}" alt="" class="imgcontent"></a>
                        @if(($row->promotion !==0) )
                                <p class="blink">Khuyến Mãi: {{number_format($row->promotion)}} ₫<br></p>
                                <p style="font-family: cursive;color:red;">Giá Bán: <del>{{number_format($row->price)}} ₫</del><br></p>
                                @else
                                <p>&nbsp;</p>
                                <p style="font-family: cursive;color:blue;">Giá Bán: {{number_format($row->price)}} ₫<br></p>
                            
                        @endif
                    <a href="{{route('detailProducts',$row->id)}}">Xem Chi Tiết</a> <br>
                    
                    <button type='button' class="add_to_cad" name="add_to_cad" data-id='{{$row->id}}'>Thêm vào giỏ</button>
                </div>
            </div>
            @endforeach
         
    </div>
    <!-- News - Events -->
    <div class="container mt-50">
        <div class="category-title">
            <p>TIN TỨC - SỰ KIỆN</p>
        </div>
        <div class="row">
        @foreach($news as $row)
            <div class="col-md-4 col-12 no-p-left">
                <article class="news">
                    <a href="" id="casio-prize"><img src="{{ asset('upload/image/news/'.$row->image) }}" style="width:340px;height:210px" alt=""></a>
                    <div class="news-content">
                        <a href="" class="title">
                            {{$row->article}}
                        </a>
                        <div class="content">
                            {!!  substr(strip_tags($row->content), 0, 150) !!}
                        </div>
                    </div>
                    <div class="readmore">
                        <a href="{{route('NewsDetail',$row->id)}}"><span class="fa fa-angle-double-right"></span> Xem chi tiết...</a>
                    </div>
                </article>
            </div>
            @endforeach
        </div>
    </div>
@endsection