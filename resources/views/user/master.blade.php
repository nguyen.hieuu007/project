<!DOCTYPE html>
<html lang="en">
    <style>
        button.add_to_cad{
            font-size: 20px;
            background: #ff4013cf;
            font-family: Times, Times New Roman, Georgia, serif;
            color: white;
        }
    </style>
<head>
    @include('user.css')
    <title>Huy's Music</title>
    <base href="{{asset('')}}">
    <link href="{{asset('')}}asset/user/icons/a.jpg" rel="shortcut icon">
</head>

<body>
    @include('user.header')

    @yield('content')

    @include('user.footer')

    
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script>
    $(document).ready(function () {
        $('#menu-btn').click(function () {
            $('.menu-collapse').toggle(500);
        })
        $('.dropdownbtn').click(function () {
            $(this).children('span').delay(200).toggle(200);
            $(this).next().toggle(500);
            $(this).parent().toggleClass('divider');
            $(this).prev().toggleClass('underline');
        })
    })
</script>
<script type="text/javascript">

$(document).ready(function(){
        $('.blink').each(function() {
            var elem = $(this);
            setInterval(function() {
                if (elem.css('visibility') == 'hidden') {
                    elem.css('visibility', 'visible');
                } else {
                    elem.css('visibility', 'hidden');
                }    
            }, 500);
        });
    });

</script>

</html>