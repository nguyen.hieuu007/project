
<!-- endslide -->
<header class="">
        <!-- Infomation -->
        <div class="bg-dark">
            <div class="container">
                <div class="info" >
                    <div class="boss" style="margin:5px">
                        <span class="pr-3">Mr. Hiếu</span>
                        <a href="mailto:nguyen.hieuu007@gmail.com" id="contact-mail">nguyen.hieuu007@gmail.com</a>
                        <a href="tel:+840966131336" id="contact-phone">09.6613.1336</a>
                    </div>
                    <div class="about-us" align="center">
                        <!-- <div class="row"> -->
                            <!-- <div class="col-lg-5 col-12"> -->
                                <!-- <a href="">Giới thiệu</a>
                                <a href="">Liên hệ</a> -->
                            <!-- </div>
                            <div class="col-lg-7 col-12"> -->
                                <form style="margin:5px" action="{{route('search')}}" method="get">
                                    <input type="search"  name="key" class="search_query" placeholder="Tìm kiếm tên sản phẩm">
                                    <button class="button" type="submit"><span class="fa fa-search"></span></button>
                                </form>
                            <!-- </div> -->
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="shadow-bottom">
            <div class="container">
                <div class="row">
                    <!-- Logo -->
                    <div class="col-lg-3 col-6 logo">
                        <a href="{{route('index')}}"><img src="{{asset('')}}asset/user/logo/logo.png" alt="" class="mw-100"></a>
                    </div>
                    <!-- Menu -->
                    <div class="col-lg-9 col-6 menu">
                        <ul class="navbar-nav navbar-expand d-none d-lg-flex flex-wrap" id="menu">
                            @foreach($category as $row)
                            <li class="nav-item">
                                <a href="{{route('products_user',$row->id)}}" class="nav-link">{{$row->name}}</a>
                                
                                @if(count($row->category_child) >0)
                                <ul class="submenu">
                                    @foreach($row->category_child as $row_child)
                                        <li class="nav-item"><a href="{{route('listProducts_user', $row_child->id)}}" class="nav-link">{{$row_child->name}}</a></li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                            <li class="nav-item"><a href="{{route('promotion')}}" class="nav-link">KHUYẾN MẠI</a></li>
                            <li class="nav-item">
                                <a href="{{route('news')}}" class="nav-link">TIN TỨC</a>
                            </li>
                        </ul>

                        <!-- Mobile menu -->
                        <div class="d-block d-lg-none" style="z-index: 10000000;">
                            <button id="menu-btn"><img src="{{asset('')}}asset/user/icons/menu-icon.png" alt="" class="menu-bars"></button>
                            <ul class="menu-collapse navbar-collapse">
                                <!-- <li class="nav-item">
                                    <a href="" class="nav-link">GUITAR</a>
                                    <button class="dropdownbtn">
                                        <span class="fa fa-angle-down"></span>
                                        <span class="fa fa-angle-up"></span>
                                    </button>
                                    <ul class="submenu1">
                                        <li class="nav-item"><a href="" class="nav-link"></a></li>
                                    </ul>
                                </li> -->
                                @foreach($category as $row)
                                <li class="nav-item">
                                    <a href="{{route('products_user',$row->id)}}" class="nav-link">{{$row->name}}</a>
                                    <button class="dropdownbtn">
                                        <span class="fa fa-angle-down"></span>
                                        <span class="fa fa-angle-up"></span>
                                    </button>
                                    @if(count($row->category_child) >0)
                                    <ul class="submenu1">
                                        @foreach($row->category_child as $row_child)
                                            <li class="nav-item"><a href="{{route('listProducts_user', $row_child->id)}}" class="nav-link">{{$row_child->name}}</a></li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                            @endforeach
                                <!-- <li class="nav-item">
                                    <a href="" class="nav-link">PIANO</a>
                                    <button class="dropdownbtn">
                                        <span class="fa fa-angle-down"></span>
                                        <span class="fa fa-angle-up"></span>
                                    </button>
                                    <ul class="submenu1">
                                        <li class="nav-item"><a href="" class="nav-link"></a></li>
                                    </ul>
                                </li>
                                <li class="nav-item"><a href="" class="nav-link">UKULELE</a></li>
                                <li class="nav-item"><a href="" class="nav-link">KHUYẾN MẠI</a></li>
                                <li class="nav-item">
                                    <a href="" class="nav-link">KHÁC</a>
                                    <button class="dropdownbtn">
                                        <span class="fa fa-angle-down"></span>
                                        <span class="fa fa-angle-up"></span>
                                    </button>
                                    <ul class="submenu1">
                                        <li class="nav-item"><a href="" class="nav-link"></a></li>
                                    </ul>
                                </li>
                                <li class="nav-item"><a href="" class="nav-link">PHỤ KIỆN</a></li>-->
                                <li class="nav-item">
                                    <a href="{{route('news')}}" class="nav-link">TIN TỨC</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('user.add_to_cart')
    </header>
    <!-- slide -->
    <!-- Banner -->
    <div id="banners" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#banners" data-slide-to="0" class="active"></li>
            <li data-target="#banners" data-slide-to="1"></li>
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{asset('')}}asset/user/banners/ClassicGuitar.png" alt="Los Angeles" class="mw-100">
            </div>
            <div class="carousel-item">
                <img src="{{asset('')}}asset/user/banners/piano.png" alt="Chicago" class="mw-100">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#banners" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#banners" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div>
