<!DOCTYPE html>
<html>

<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="{{asset('')}}asset/admin/zoom/css/normalize.css" />
        <link rel="stylesheet" href="{{asset('')}}asset/admin/zoom/css/foundation.css" />
        <link rel="stylesheet" href="{{asset('')}}asset/admin/zoom/css/demo.css" />
        <script src="{{asset('')}}asset/admin/zoom/js/vendor/modernizr.js"></script>
        <script src="{{asset('')}}asset/admin/zoom/js/vendor/jquery.js"></script>
        <!-- ------------- -->
        <script type="text/javascript" src="{{asset('')}}asset/admin/zoom//dist/xzoom.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{{asset('')}}asset/admin/zoom/css/xzoom.css" media="all" /> 
        
        <script src="{{asset('')}}asset/admin/zoom/js/foundation.min.js"></script>
        <script src="{{asset('')}}asset/admin/zoom/js/setup.js"></script>
        <link href="{{asset('')}}asset/user/icons/a.jpg" rel="shortcut icon">
    <style>
        button.add_to_cad{
            font-size: 20px;
            background: #ff4013cf;
            font-family: Times, Times New Roman, Georgia, serif;
            color: white;
        }
    </style>
    <script>

    </script>
</head>

<body>
    @extends('user.master')

    @section('content')
<br>
    <h1 class="brands" align="center">Chi Tiết</h1>

    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-sm-12 col-12">
                
                <section id="default" class="padding-top0">

                    <div class="row">
                        <div class="large-10 column" align="center">
                            <h3 style="color: brown;text-transform: uppercase;margin-top:5px"> {{$productsDetail->product_name}}</h3>
                            <div class="xzoom-container">
                                <img class="xzoom" id="xzoom-default" style="max-width:400px;min-height:200px" src="{{ asset('upload/image/products/'.$productsDetail->image) }}"
                                    xoriginal="{{ asset('upload/image/products/'.$productsDetail->image) }}" />
                                <div class="xzoom-thumbs">
                                    <a href="{{ asset('upload/image/products/'.$productsDetail->image) }}"><img class="xzoom-gallery" width="80" src="{{ asset('upload/image/products/'.$productsDetail->image) }}"
                                            title="color : éc éc"></a>
                                    @foreach($products_img as $row)
                                    <a href="{{ asset('upload/image/products/'.$row->url_images) }}"><img class="xzoom-gallery" width="80" src="{{ asset('upload/image/products/'.$row->url_images) }}"
                                            title="color : éc éc"></a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="large-2 column"></div>
                    </div>
                </section>
            </div>
            <div class="col-lg-6 col-sm-12 col-12">
                
                <div style="border: 2px solid #c5b13e00;border-radius: 15px;padding:5px;margin-top:50px" align="center">

                        @if(($productsDetail->promotion !== 0) )
                            <p class="blink" style="font-size:20px">Khuyến Mãi: {{number_format($productsDetail->promotion)}} ₫<br></p>
                            <p style="font-family: cursive;color:red;">Giá Bán: <del>{{number_format($productsDetail->price)}} ₫</del><br></p>
                        @else
                            <p>&nbsp;</p>
                            <p style="font-family: cursive;color:blue;font-size:20px">Giá Bán: {{number_format($productsDetail->price)}} ₫<br></p>
                        @endif
                        
                        <div>
                                
                            <button type='button' class="add_to_cad" name="add_to_cad" data-id='{{$productsDetail->id}}'>Thêm vào giỏ</button>
                        </div>

                        <div style="margin:20px">
                            <div>{!!$productsDetail->infor !!}</div>
                        </div>
<!-- -------------------- -->

<!-- ---------------------- -->
                </div>
            </div>
        </div>
    </div>
    




    @endsection


</body>








</html>