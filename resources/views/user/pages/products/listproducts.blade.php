    <style>
        button.add_to_cad{
            font-size: 20px;
            background: #ff4013cf;
            font-family: Times, Times New Roman, Georgia, serif;
            color: white;
        }

    </style>
@extends('user.master')

@section('content')

@foreach($products as $row)
<h1 class="brands">{{$row->name}}</h1>
@endforeach

<div class="container mt-50">
    <div class="category-title">
        <p>SẢN PHẨM</p>
    </div>
    <div class="row stand-out-products">
        @foreach($productsList as $row)
        <div class="col-lg-3 col-sm-6 col-12">
            <div style="border: 2px solid #c5b13e00;border-radius: 15px;padding:5px">
                <h5 style="color: brown;text-transform: uppercase;margin-top:5px;font-size:19px">{{$row->product_name}}</h5>
                <a href="{{route('detailProducts',$row->id)}}"><img src="{{ asset('upload/image/products/'.$row->image) }}" alt="" class="imgcontent"></a>
                    @if(($row->promotion !==0) )
                        <p class="blink">Khuyến Mãi: {{number_format($row->promotion)}} ₫ <br></p>
                        <p style="font-family: cursive;color:red;">Giá Bán: <del>{{number_format($row->price)}} ₫</del><br></p>
                        @else
                        <p>&nbsp;</p>
                        <p style="font-family: cursive;color:blue;">Giá Bán: {{number_format($row->price)}} ₫<br></p>
                    @endif
                <a href="{{route('detailProducts',$row->id)}}" style="text-decoration:none;color:darkviolet">Xem Chi Tiết</a> <br>
<!-- -----------cart -->
                <button type='button' class="add_to_cad" name="add_to_cad" data-id='{{$row->id}}'>Thêm vào giỏ</button>
            
            
            </div>
        </div>
        @endforeach
    </div>
</div>
    <div class="d-flex justify-content-end" style="margin:30px;">
        <div class="btn-group">
        {{$productsList->links()}}
        </div>
    </div>

@endsection




