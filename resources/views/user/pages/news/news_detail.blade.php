
<style>
    .news_container{
        margin:20px;
    }

</style>

@extends('user.master')

@section('content')


<div class="container">

    @foreach($newsDetail as $row)
    <div><h2 class="brands">TIN TỨC <p style="color:blue">{{$row->article}}</p></h2> </div>
    <div class="news_container">
        <img src="{{ asset('upload/image/news/'.$row->image) }}" style="width:300px;float:right;" alt="">
        <div>{!!$row->content!!}</div>
        <small>{{$row->created_at}}</small>

    </div>
    <hr>
    @endforeach


</div>
@endsection