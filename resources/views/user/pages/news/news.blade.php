

<style>
    .news_container{
        margin:20px;
    }

</style>

@extends('user.master')

@section('content')
<h1 class="brands">TIN TỨC</h1>

<div class="container">

    @foreach($news as $row)
    <div class="news_container">
        <h1 style="color:blue">{{$row->article}}</h1>
        <img src="{{ asset('upload/image/news/'.$row->image) }}" style="width:300px;float:right;" alt="">
        <div>{!!$row->content!!}</div>
        <small>{{$row->created_at}}</small>

    </div>
    <hr>
    @endforeach


</div>
@endsection