<head>
    <link href="{{asset('')}}asset/user/Cart/cart.css" type="text/css" rel="stylesheet">
    <title>Huy's Music</title>
    <base href="{{asset('')}}">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    
</head>

<body>

    <div style="margin:90" class="container ">
        <div class="page_cart ">
            <div class="container ">
                <div class="step_payment">
                    Bước 1: Xem đơn hàng và tính toán số lượng cần thiết
                </div>
                <div id='form-id' name="frmCart">
                    <input type="hidden" name="frame" value="giohang">
                    <table class="info-product" align="center">
                        <tr align="center">
                            <th width="5%" class="hidden-xs hidden-sm"></th>
                            <th width="25%">Tên sản phẩm</th>
                            {{--<th width="25%" class="hidden-xs hidden-sm">Kiểu tính</th>--}}
                            <th width="10%" class="hidden-xs hidden-sm">S.lượng</th>
                            <th width="10%">Đ.vị</th>
                            <th width="20%">Đơn giá</th>
                            <th width="15%">Thành tiền</th>
                            <th width="5%">Xóa</th>
                        </tr>

                        @foreach(Cart::content() as $key => $row)

                        <tr align="center">
                            <td class="hidden-xs hidden-sm"></td>
                            <td><img src="{{asset('')}}upload/image/products/{{$row->options['image']}}" class="img_pd"
                                    style="width:100px;display:inline-block" /><br /><br><span>
                                    <a href="{{route('getDetailProducts', $row->id)}}" style="margin:1px;">
                                        {{$row->name}}
                                    </a></span>
                            </td>

                            <td>
                                <font color="#FF0000">
                                    <input style="width: 40px;height: 30px;padding: 5px;" id="numberbuy" data-id="{{$row->rowId}}"
                                        class="quantity" type="number" step="1" min="1" name="txtQuantity[]" value="{{$row->qty}}"
                                        title="Qty" size="4">
                                </font>
                            </td>
                            <td class="hidden-xs hidden-sm">
                                <font color="#FF0000">SP</font>
                            </td>
                            <td>
                                <font color="#FF0000">
                                    @if(isset($row->options['promotion']) && ($row->options['promotion'] !== 0))
                                    {{ number_format($row->options['promotion'])}}₫
                                    <div style='height: 55px;text-decoration: line-through; color: #b2b2b2; font-weight: normal; font-size: 11px;   margin-bottom: -35px;'>GNY:
                                        {{number_format(@$row->options['_price'])}}₫</div>
                                    @else
                                    {{ number_format($row->options['_price'])}}₫
                                    @endif
                                </font>

                            </td>

                            <td>
                                <font color="#FF0000">
                                    <lable id="{{$row->rowId}}">{{number_format($row->price * $row->qty)}}</lable>₫
                                </font>
                            </td>

                            <td>
                                <input data-id="{{$row->rowId}}" type="button" class="buttonorange" style="width:50px"
                                    value="Xóa">
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td class="hidden-xs hidden-sm" align="right" class="title"></td>
                            <td class="title"></td>
                            <td align="right" class="title"></td>
                            <td class="hidden-xs hidden-sm" align="right" class="title"></td>
                            <td align="left" class="title"><b>Phí ship: </b></td>
                            <td align="left" class="title">
                                <font color="#FF0000"><b>Free Ship nội thành</b></font>
                            </td>
                            <td align="right" class="title"></td>
                        </tr>
                        <tr>
                            <td align="right" class="hidden-xs hidden-sm" class="title"></td>
                            <td align="right" class="title"></td>
                            <td align="right" class="title"></td>
                            <td align="right" class="hidden-xs hidden-sm" class="title"></td>
                            <td align="center" class="title" ><b style="font-size: 15px;">Thanh toán: </b></td>
                            <td align="left" class="title">
                                <font color="#FF0000"><b><label id="subtotal" style="font-size: 15px;">{{(Cart::subtotal())}}</label>
                                        <font color="#FF0000">₫</font>
                                    </b></font>
                            </td>
                            <td align="right" class="title"></td>
                        </tr>
                    </table>
                </div>

                <div class="step_payment">
                    Bước 2: Lựa chọn phương thức thanh toán <img src="{{asset('')}}asset/user/images/cart_direct.png"
                        alt=" Thanh toán trực tiếp khi giao hàng" />
                </div>
                <div class="col-md-12 top-info" style="min-height: 150px;">
                    <p class="col-md-12" style="text-align: center;margin-top: 35px;">
                        <input name="btnCheckout" type="image" src="{{asset('')}}asset/user/images/online_vn.jpg"
                            value="Thanh toán ngay" alt="" />

                        
                    </p>
                </div>
                <!-- --------------------------- -->
                <div>
                    <div class="page_cart">
                        <div class="container">
                            <div class="step_payment">
                                Bước 3: Điền thông tin thanh toán
                            </div>
                            
                            <span class="cart_inline">
                                Mời quý khách điền các thông tin cá nhân bên dưới để nhân viên chúng tôi liên hệ với
                                quý khách.</span>
                            <ul class="payment_last">
                                <li>
                                    <span class="pay_left">
                                        <img src="{{asset('')}}asset/user/images/cart_direct.png" alt=" Đặt hàng trực tuyến">
                                    </span>
                                    <span class="pay_right">
                                        <input type="radio" name="payment_type" value="direct" checked="checked"> Đặt
                                        hàng trực tuyến </span>
                                </li>
                            </ul>
                            <div class="col-md-12 offset-0">
                                <div class="col-md-5 offset-0 borderright">
                                    <table style="    margin-top: -5px;" border="0" cellspacing="5" cellpadding="0"
                                        width="100%" align="center" bordercolor="#000000">
                                        
                                        <tbody>
                                            
                                            <input type="hidden" name="frame" value="dienthongtin">
                                            <input type="hidden" name="type" value="1">
                                            <input type="hidden" name="tensanpham" value="">
                                            <input type="hidden" name="tongtien" value="">
                                            <tr>
                                                <td height="20" colspan="4" class="no-border" style="padding-left:5px">
                                                    <b>Thông tin cá nhân (</b><i>Chú ý : <font color="#FF0000">* </font>là
                                                        thông tin phải nhập )</i>
                                                    <hr size="1" noshade="">
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="right" class="no-border" width="45%">Kiểu đặt hàng</td>
                                                <td class="no-border" width="5%" align="center">:</td>
                                                <td class="no-border" nowrap="" width="45%"><input class="fieldKey no-border upcase" size="33"  value="Đặt hàng trực tuyến" readonly=""></td>
                                                <td class="no-border" nowrap="" width="5%"></td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="no-border" width="45%">Họ tên</td>
                                                <td class="no-border" width="5%" align="center">:</td>
                                                <td class="no-border" nowrap="" width="45%"><input class="fieldKey" id="txtFullname" size="33" name="txtFullname" value="" required></td>
                                                <td class="no-border" nowrap="" width="5%">
                                                    <font color="#FF0000">*</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="no-border" width="45%">Địa chỉ</td>
                                                <td class="no-border" width="5%" align="center">:</td>
                                                <td class="no-border" nowrap="" width="45%"><input class="fieldKey" id="txtAddress" size="33" name="txtAddress" value="" required></td>
                                                <td class="no-border" nowrap="" width="5%">
                                                    <font color="#FF0000">*</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="no-border" align="right" width="45%">Số điện thoại</td>
                                                <td class="no-border" width="5%" align="center">:</td>
                                                <td class="no-border" nowrap="" width="45%"><input class="fieldKey" id="txtTel" size="33" name="txtTel" value="" required></td>
                                                <td class="no-border" nowrap="" width="5%">
                                                    <font color="#FF0000">*</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="no-border" align="right" width="45%">Email</td>
                                                <td class="no-border" width="5%" align="center">:</td>
                                                <td class="no-border" nowrap="" width="45%"><input class="fieldKey" id="txtEmail" size="33" name="txtEmail" value=""></td>
                                                <td class="no-border" nowrap="" width="5%">
                                                    <font color="#FF0000"></font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="no-border" align="right" width="33%"></td>
                                                <td class="no-border" width="45" align="center"> </td>
                                                <td class="no-border" nowrap="" width="27%">
                                                    <div class="page_cart" style="margin: 0px;">
                                                        <div class="submit_cart">
                                                            <input name="btnHoantat" id="submit" type="submit" value="Xác nhận thông tin" alt="">

                                                        </div>
                                                </td>
                                                <td class="no-border" nowrap="" width="36%">
                                                    <font color="#FF0000"></font>
                                                </td>
                                            </tr>
                                        </tbody>
                                       
                                    </table>
                                </div>
                                <!-- ---------------------------------- -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>



<script>
    $(document).on('change', '.quantity', function () {
        var id = $(this).attr('data-id');
        var value = $(this).val();
        if (value > 0) {
            $.ajax({
                type: "GET",
                url: window.location.origin + '/update-cart/' + id + '/' + value,
                success: function (data) {
                    $('#' + id).html(data['total_price']);
                    $('#subtotal').html(data['total']);
                }
            })
        }
    });

    $(document).on('click', '.buttonorange', function () {
        var id = $(this).attr('data-id');

        $.ajax({
            type: "GET",
            url: window.location.origin + '/delete-cart/' + id,
            success: function (data) {
                if (data == 1) {
                    window.location.reload();
                }
            }
        })
    });
    </script>


<script>
    $(document).ready(function () {
        $("input#submit").click(function () {
            if ($('#txtFullname').val() == '') {
                alert('Vui lòng nhập họ tên đầy đủ.');
                return false;
            }

            if ($('#txtAddress').val() == '') {
                alert('Vui lòng nhập địa chỉ của bạn.');
                return false;
            }

            if ($('#txtTel').val() == '') {
                alert('Vui lòng nhập Số điện thoại của bạn.');
                return false;
            }

            var regex2 = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
            if (!regex2.test($('#txtTel').val())) {
                alert('Số điện thoại không chính xác.');
                return false;
            }
                // var full_name= $('#txtFullname').val();
                // alert(full_name);
            $.ajax({
                type: "get",
                url: window.location.origin + '/thanh-toan-san-pham',
                dataType:'json',
                data: {
                    full_name: $('#txtFullname').val(),
                    address: $('#txtAddress').val(),
                    phone: $('#txtTel').val(),
                    email: $('#txtEmail').val()
                },
                
                
                // var_dump();
                // die();
                
                success: function (data1) {
                    if (data1 == 1) {
                        alert("Bạn đã đặt hàng thành công");
                        window.location.href = window.location.origin;
                    } else {
                        alert("Vui lòng thêm sản phẩm vào giỏ hàng");
                    }
                }
            })
        });
    });
</script>




