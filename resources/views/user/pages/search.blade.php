@extends('user.master')
<style>
    .cat-sort{
        display: inline-block;
        /* background: #c5b13e; */
        margin: 10px;
        padding: 10px;
        border-style: dotted dashed solid double;
        border-color: #c5b13e;
        border-radius: 20px;
    }
    .cat-sort label{
        color: #a99212;
        font-size: 25px;
        font-family: none;
    }
    .selectSearch {
        width: 250px;
        margin: 10px;
        color: coral;
        padding: 5px;
        border: 1px solid;
        border-radius: 10px;
    }
</style>
@section('content')
    <h1 class="brands">TÌM KIẾM</h1>
        <div class="container">

            <div role="navigation" align="right">
                <div class="cat-sort">
                    <label >Sắp xếp theo</label>
                    <select class="selectSearch" onchange="location = this.value;" class="sort-option sort-watch">
                        <option value="tim-kiem?key={{$key}}">Thứ tự mặc định</option>
                        <option value="tim-kiem?order=price&status=asc&key={{$key}}" @if($status == 'asc') selected @endif>Theo giá: thấp đến cao</option>
                        <option value="tim-kiem?order=price&status=desc&key={{$key}}" @if($status == 'desc') selected @endif>Theo giá: cao xuống thấp</option>
                    </select>
                    
                </div>
            </div>


            <div class="row stand-out-products">
                @foreach($product as $row)
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div style="border: 2px solid #c5b13e00;border-radius: 15px;padding:5px">
                            <h5 style="padding: 5px;color: brown;text-transform: uppercase;margin-top:5px">{{$row->product_name}}</h5>
                            <a href="{{route('detailProducts',$row->id)}}"><img src="{{ asset('upload/image/products/'.$row->image) }}" alt="" class="imgcontent"></a>
                                @if(($row->promotion !==0) )
                                    <p class="blink">Khuyến Mãi: {{number_format($row->promotion)}}₫ <br></p>
                                    <p style="font-family: cursive;color:red;">Giá Bán: <del>{{number_format($row->price)}}₫</del><br></p>
                                    @else
                                    <p>&nbsp;</p>
                                    <p style="font-family: cursive;color:blue;">Giá Bán: {{number_format($row->price)}}₫<br></p>
                                    
                                @endif
                            <a href="{{route('detailProducts',$row->id)}}">Xem Chi Tiết</a> <br>
                            
                            <button type='button' class="add_to_cad" name="add_to_cad" data-id='{{$row->id}}'>Thêm vào giỏ</button>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="d-flex justify-content-end" style="margin:30px;">
                <div class="btn-group">
                    {{$product->links()}}
                </div>
            </div>
            @if( $order !== '' && $status !== '')
                {{ $product->appends(array('order' => $order, 'status' => $status, 'key' => $key ))->render() }}
            @else
                {{ $product->appends(array('key' => $key))->render() }}
            @endif
        </div>
@endsection