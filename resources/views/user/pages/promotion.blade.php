    @extends('user.master')

    @section('content')
        <h1 class="brands">KHUYẾN MÃI</h1>
            <div class="container">
                <div class="row stand-out-products">
                    @foreach($promotion as $row)
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div style="border: 2px solid #c5b13e00;border-radius: 15px;padding:5px">
                                <h5 style="padding: 5px;color: brown;text-transform: uppercase;margin-top:5px">{{$row->product_name}}</h5>
                                <a href="{{route('detailProducts',$row->id)}}"><img src="{{ asset('upload/image/products/'.$row->image) }}" alt="" class="imgcontent"></a>
                                    @if(($row->promotion !==0) )
                                        <p class="blink">Khuyến Mãi: {{$row->promotion}} <br></p>
                                        <p style="font-family: cursive;color:red;">Giá Bán: <del>{{$row->price}}</del><br></p>
                                        @else
                                        <p>&nbsp;</p>
                                        <p style="font-family: cursive;color:blue;">Giá Bán: {{$row->price}}<br></p>
                                        
                                    @endif
                                <a href="{{route('detailProducts',$row->id)}}">Xem Chi Tiết</a> <br>
                                
                                <button type='button' class="add_to_cad" name="add_to_cad" data-id='{{$row->id}}'>Thêm vào giỏ</button>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-end" style="margin:30px;">
                    <div class="btn-group">
                        {{$promotion->links()}}
                    </div>
                </div>
            </div>
    @endsection