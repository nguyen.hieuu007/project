
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('admin.css')
    <title>Huy's Music</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div  style="background:tomato;padding:10px" class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12 box ">
                <h2 class="title">Đăng nhập</h2>
                <form action="{{route('adminPostLogin')}}" class="px-3 pt-3 w-100 " id="form" method="POST">

                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    @if(count($errors) >0)
                    <div class="alert alert-danger">
                        @foreach($errors-> all() as $err)
                            {{$err}}<br>
                        @endforeach
                    </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-warning">
                            {{session('error')}}
                        </div>
                    @endif

                    <div class="form-group position-relative">
                        <label>Tài khoản</label>
                        <input type="email" name="email" class="form-control" id="username"/>
                    </div>
                    <div class="form-group position-relative">
                        <label for="password">Mật khẩu</label>
                        <input type="password" name="password" class="form-control" id="password"/>
                    </div>
                    <div class="d-flex justify-content-center">
                        <input type="submit" value="Đăng nhập" id="submit" class="btn btn-outline-secondary"/>   
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
