@extends('admin.master')

@section('Admincontent')

    <div class="row">

        
        <div class="col-md-4 col-sm-6 col-xs-12" align="center">
            <a style="text-decoration: none;"  href="{{route('billCheck')}}">
                <div class="info-box" style="background: #fff3f3;border: 1px solid rgb(252, 237, 214);">
                    <span class="info-box-icon bg-red"><i class="fa fa-cart-plus" style="font-size: 100px; color:coral;margin:5px"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Đơn đã duyệt</span>
                        <span class="info-box-number">{{$bill_check}}</span>
                    </div>
                </div>
            </a>
        </div>
        
        <div class="col-md-4 col-sm-6 col-xs-12" align="center">
            <a style="text-decoration: none;"  href="{{route('billTemp')}}">
                <div class="info-box" style="background: #fdffad;border: 1px solid rgb(252, 237, 214);">
                    <span class="info-box-icon bg-red"><i class="fas fa-cart-arrow-down" style="font-size: 100px; color: darkviolet ;margin:5px"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Đơn chưa duyệt</span>
                        <span class="info-box-number">{{$bill}}</span>
                    </div>
                </div>
            </a>
        </div>
        

        <div class="col-md-4 col-sm-6 col-xs-12" align="center">
            <a style="text-decoration: none;"  href="{{route('products')}}">
                <div class="info-box" style="background: #dc3535;border: 1px solid rgb(252, 237, 214);">
                    <span class="info-box-icon bg-red"><i class="fab fa-phoenix-framework" style="font-size: 100px; color: floralwhite;margin:5px"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text" style="color:white;">Sản Phẩm</span>
                        <span class="info-box-number" style="color:white;">{{$product}}</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12" align="center">
            <a style="text-decoration: none;"  href="{{route('listUser')}}">
                <div class="info-box" style="background: #ffd4a7;border: 1px solid rgb(252, 237, 214);">
                    <span class="info-box-icon bg-red"><i class="fab fa-studiovinari" style="font-size: 100px; color: #880202;margin:5px"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Admin</span>
                        <span class="info-box-number">{{$user}}</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12" align="center">
            <a style="text-decoration: none;" href="{{route('listCustomer')}}">
                <div class="info-box" style="background: #dc3579;border: 1px solid rgb(252, 237, 214);">
                    <span class="info-box-icon bg-red"><i class="fas fa-universal-access" style="font-size: 100px; color: floralwhite;margin:5px"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text" style="color:white;">Khách Hàng</span>
                        <span class="info-box-number" style="color:white;">{{$customer}}</span>
                    </div>
                </div>
            </a>
        </div>
        
    </div>


@endsection