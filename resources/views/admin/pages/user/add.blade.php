
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">User add</div>
        </div>
        <div class="mt-3">
            @if(count($errors) >0)
                <div class="alert alert-danger">
                    @foreach($errors-> all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif

            @if(session('thongbao'))
                <div class="alert alert-danger">
                    {{session('thongbao')}}
                </div>
            @endif
            <form action="{{route('postAddUser')}}" method="post">
                @csrf
                <div class="form-group">
                    <b><label>Name</label></b>
                    <input type="text" class="form-control" name="user_name" />
                </div>
                <div class="form-group">
                    <b><label>Email</label></b>
                    <input type="email" class="form-control" name="user_email" />
                </div><div class="form-group">
                    <b><label>PassWord</label></b>
                    <input type="password" class="form-control" name="user_pass" />
                </div>
                <button type="submit" class="bnt btn-default">User Add</button>
                <button type="reset" class="bnt btn-default">Reset</button>
            </form>
        </div>
    </div>
@endsection