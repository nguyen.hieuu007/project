
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">User Exit <small><small>{{$userEdit->name}}</small></small> </div>
        </div>
        <div class="mt-3">
            @if(count($errors) >0)
                <div class='alert alert-danger'>
                    @foreach($errors->all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif

            @if(session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
            @endif
            
            <form action="{{route('postEditUser',$userEdit->id)}}" method="post">
                @csrf
                <div class="form-group">
                    <b><label>Name <small></small></label></b>
                    <input type="text" class="form-control" name="user_name" value="{{$userEdit->name}}"/>
                </div>
                <div class="form-group">
                    <b><label>Email <small></small></label></b>
                    <input type="email" class="form-control" name="user_email" value="{{$userEdit->email}}"/>
                </div>
                <div class="form-group">
                    <b><label>Pass Word <small></small></label></b>
                    <input type="password" class="form-control" name="user_pass" value="{{$userEdit->password}}" />
                </div>
                <button type="submit" class="bnt btn-default">User Edit</button>
                <button type="reset" class="bnt btn-default">Reset</button>
            </form>
        </div>
    </div>
@endsection