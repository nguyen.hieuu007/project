
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">User list</div>
            <a href="{{route('getAddUser')}}"><button class="btn btn-light">Add User</button></a>
        </div>
        <div class="d-flex justify-content-between mt-3">
            <div>
                <p style={display: 'inline'}>
                    Show 5 entries</p>
            </div>
        </div>
        @if(session('thongbao'))
            <div class="alert alert-success">
                {{session('thongbao')}}
            </div>
        @endif
        <div class="mt-3">
            <table class="table table-bordered" >
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th><span class="fab fa-whmcs" style={color:tomato}>  </span>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($userList as $row)
                    <tr align="center">
                        <td>{{$row->id}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->email}}</td>
                        <td><a href="{{route('getEditUser',$row->id)}}" ><button type="button" class="btn btn-primary btn-sm">Edit</button></a></td>
                        <td><a href="{{route('getDeleteUser',$row->id)}}"><button type="button" class="btn btn-primary btn-sm">Delete</button></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
            <div class="d-flex justify-content-end">
                <div class="btn-group">
                {{$userList->links()}}
                </div>
            </div>
        </div>
    </div>
    
@endsection