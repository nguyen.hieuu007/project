
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">Category Exit </div>
        </div>
        <div class="mt-3">
            @if(count($errors) >0)
                <div class='alert alert-danger'>
                    @foreach($errors->all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif

            @if(session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
            @endif
            
            <form action="{{route('postEditCategory',$categoryEdit->id)}}" method="post">
                @csrf
                <div class="form-group">
                    <b><label>Name <small>{{$categoryEdit->name}}</small></label></b>
                    <input type="text" class="form-control" name="category_child" value="{{$categoryEdit->name}}"/>
                </div>
                <button type="submit" class="bnt btn-default">Category Exit</button>
                <button type="reset" class="bnt btn-default">Reset</button>
            </form>
        </div>
    </div>
@endsection