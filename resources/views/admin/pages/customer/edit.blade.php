
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">Customer Exit <small><small>{{$customerEdit->name}}</small></small> </div>
        </div>
        <div class="mt-3">
            @if(count($errors) >0)
                <div class='alert alert-danger'>
                    @foreach($errors->all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif

            @if(session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
            @endif
            
            <form action="{{route('postEditCustomer',$customerEdit->id)}}" method="post">
                @csrf
                <div class="form-group">
                    <b><label>Name <small></small></label></b>
                    <input type="text" class="form-control" name="customer_name" value="{{$customerEdit->name}}"/>
                </div>
                <div class="form-group">
                    <b><label>Phone <small></small></label></b>
                    <input type="text" class="form-control" name="customer_phone" value="{{$customerEdit->phone}}"/>
                </div>
                <div class="form-group">
                    <b><label>Email <small></small></label></b>
                    <input type="email" class="form-control" name="customer_email" value="{{$customerEdit->email}}"/>
                </div>
                <div class="form-group">
                    <b><label>Address <small></small></label></b>
                    <input type="address" class="form-control" name="customer_address" value="{{$customerEdit->address}}" />
                </div>
                <button type="submit" class="bnt btn-default">Customer Edit</button>
                <button type="reset" class="bnt btn-default">Reset</button>
            </form>
        </div>
    </div>
@endsection