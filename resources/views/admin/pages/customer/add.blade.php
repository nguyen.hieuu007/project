
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">Customer add</div>
        </div>
        <div class="mt-3">
            @if(count($errors) >0)
                <div class="alert alert-danger">
                    @foreach($errors-> all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif

            @if(session('thongbao'))
                <div class="alert alert-danger">
                    {{session('thongbao')}}
                </div>
            @endif
            <form action="{{route('postAddCustomer')}}" method="post">
                @csrf
                <div class="form-group">
                    <b><label>Name</label></b>
                    <input type="text" class="form-control" name="customer_name" />
                </div>
                <div class="form-group">
                    <b><label>Phone</label></b>
                    <input type="text" class="form-control" name="customer_phone" />
                </div>
                <div class="form-group">
                    <b><label>Email</label></b>
                    <input type="email" class="form-control" name="customer_email" />
                </div><div class="form-group">
                    <b><label>Address</label></b>
                    <input type="address" class="form-control" name="customer_address" />
                </div>
                <button type="submit" class="bnt btn-default">Customer Add</button>
                <button type="reset" class="bnt btn-default">Reset</button>
            </form>
        </div>
    </div>
@endsection