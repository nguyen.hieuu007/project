
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">Customer list</div>
            <!-- <a href="{{route('getAddCustomer')}}"><button class="btn btn-light">Add Customer</button></a> -->
        </div>
        <div class="d-flex justify-content-between mt-3">
            <div>
                <p style={display: 'inline'}>
                    Show 5 entries</p>
            </div>
            <form style="float:right;margin:5px" action="{{route('search_cutomer')}}" method="get">
                <input type="search"  name="key" class="search_query" placeholder="Tìm kiếm ">
                <button class="button" type="submit"><span class="fa fa-search"></span></button>
            </form>
        </div>
        @if(session('thongbao'))
            <div class="alert alert-success">
                {{session('thongbao')}}
            </div>
        @endif
        @if(session('loi'))
            <div class="alert alert-danger">
                {{session('loi')}}
            </div>
        @endif
        <div class="mt-3">
            <table class="table table-bordered" >
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th><span class="fab fa-whmcs" style={color:tomato}>  </span>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($customerList as $row)
                    <tr align="center">
                        <td>{{$row->id}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->phone}}</td>
                        <td>{{$row->email}}</td>
                        <td>{{$row->address}}</td>
                        <td><a href="{{route('getEditCustomer',$row->id)}}" ><button type="button" class="btn btn-primary btn-sm">Edit</button></a></td>
                        <td><a href="{{route('getDeleteCustomer',$row->id)}}"><button type="button" class="btn btn-primary btn-sm">Delete</button></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
            <div class="d-flex justify-content-end">
                <div class="btn-group">
                {{$customerList->links()}}
                </div>
            </div>
        </div>
    </div>
    
@endsection
