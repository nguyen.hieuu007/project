@extends('admin.master')

@section('Admincontent')
    <div>
        <div class="main-title">
            <div class="text-gray">Bill Detail <small><span style="color:red;">NOT Checked </span></small></div>
        </div>
        <div class="d-flex justify-content-between mt-3">
            <div>
                <p style={display: 'inline'}>
                    Show 5 entries</p>
            </div>
            <!-- <div>
                <p style={display: 'inline'}> Search: </p>
                <input style={borderRadius: '5px';} type="search" />
            </div> -->
        </div>
        <div class="mt-3">
            @if($bill->count() ==0)
            <div class="alert alert-info">
                Đã duyệt hết.
            </div>
            @endif
            @if($bill->count() !=0)
            <div class="alert alert-warning">
                Có {{$bill->count()}} hóa đơn chưa duyệt
            </div>
            @endif
            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
            <table id="example1" class="table table-bordered">
                <thead>
                    <tr align="center">
                        <th>Bill</th>
                        <th>Name</th>
                        <th>Oder day</th>
                        <th>Adress</th>
                        <th>Phone</th>
                        <th>Total</th>
                        <th>Set</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bill as $row)
                    <tr align="center">
                        <td>{{$row->id}}</td>
                        <td>{{$row->customer->name}}</td>
                        <td>{{$row->created_at}}</td>
                        <td>{{$row->customer->address}}</td>
                        <td>{{$row->customer->phone}}</td>
                        <td>{{number_format($row->total)}}</td>
                        <td>
                            <a href="{{route('billPass', $row->id)}}"><button type="button" class="btn btn-primary btn-sm">Duyệt</button></a>
                            <a href="{{route('billDetails', $row->id)}}"><button type="button" class="btn btn-primary btn-sm">ChiTiết</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-end">
                <div class="btn-group">
                {{$bill->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection