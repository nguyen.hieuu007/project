@extends('admin.master')

@section('Admincontent')
    <div>
        <div class="main-title">
            <div class="text-gray">Bill Detail</div>
        </div>
        <div class="mt-3">
            @if(session('thongbao'))
            <div class="alert alert-success">
                {{session('thongbao')}}
            </div>
            @endif
            <!-- /.box-header -->
            <div class="">
                <table id="example1" class="table table-bordered">
                    <thead>
                        <tr align="center">
                            <th>Code detailBill</th>
                            <th>CodeBill</th>
                            <th>Products</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Total</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bill_details as $row)
                        <tr align="center">
                            <td>{{$row->id}}</td>
                            <td>{{$row->bill_id}}</td>
                            <td>@if (@$row->products->product_name == '') Sản phẩm đã bị xóa @else
                                {{$row->products->product_name}} @endif</td>
                            <td>{{$row->quantity}}</td>
                            <td>{{$row->products->price}}</td>
                            <td>{{$row->total}}</td>
                        </tr>
                        @endforeach

                        </tfoot>
                        
                </table>
                <div align="right">
                    @foreach($bill_date as $row)
                        <small>Date create: {{$row->created_at}}</small><br>
                        <small> Date edit: {{$row->updated_at}}</small>
                    @endforeach
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

@endsection