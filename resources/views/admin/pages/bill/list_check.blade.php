@extends('admin.master')

@section('Admincontent')
    <div>
        <div class="main-title">
            <div class="text-gray">List Bill <small><span style="color:blue"> Checked </span></small></div>
        </div>
        <div class="d-flex justify-content-between mt-3">
            <div>
                <p style={display: 'inline'}>
                    Show 5 entries</p>
            </div>
            <!-- <form style="float:right;margin:5px"  method="get">
                <input type="search"  name="key" class="search_query" placeholder="Tìm kiếm ">
                <button class="button" type="submit"><span class="fa fa-search"></span></button>
            </form> -->
        </div>
        <div class="mt-3">
            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
            <table id="example1" class="table table-bordered">
                <thead>
                    <tr align="center">
                        <th>Bill</th>
                        <th>Name</th>
                        <th>Oder day</th>
                        <th>Adress</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Total</th>
                        <th>Set</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($bill as $row)
                    <tr align="center">
                        <td>{{$row->id}}</td>
                        <td>{{$row->customer->name}}</td>
                        <td>{{$row->created_at}}</td>
                        <td>{{$row->customer->address}}</td>
                        <td>{{$row->customer->phone}}</td>

                        <td>
                            <select id="stt_{{$row->id}}">
                                <option class="btn btn-danger" value="Chưa vận chuyển" @if($row->status_transfer === 'Chưa vận chuyển') selected @endif >Chưa vận chuyển</option>
                                <option class="btn btn-warning" value="Đang vận chuyển" @if($row->status_transfer === 'Đang vận chuyển') selected @endif >Đang vận chuyển</option>
                                <option class="btn btn-success" value="Đã vận chuyển" @if($row->status_transfer === 'Đã vận chuyển') selected @endif >Đã vận chuyển</option>
                            </select>

                        </td>
                        <td>{{number_format($row->total)}}</td>
                        <td>
                            <button type="button" class="btn btn-success btn-sm status_transfer" data-id="{{$row->id}}">Update</button>
                            <a href="{{route('billDetails', $row->id)}}"><button type="button" class="btn btn-primary btn-sm">Detail</button></a>
                            <button data-id="{{$row->id}}" type="button" class="btn btn-warning btn-sm delete-item">Del</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
<script
    src="https://code.jquery.com/jquery-1.9.1.js"
    integrity="sha256-e9gNBsAcA0DBuRWbm0oZfbiCyhjLrI6bmqAl5o+ZjUA="
    crossorigin="anonymous">
</script>

<script>
    $(document).on('click', '.status_transfer', function () {
        var id = $(this).attr('data-id');
        var stt = $('#stt_' + id).val();
        console.log(stt);
        // var value = $('#quantity_1').val();
        $.ajax({
            type: "GET",
            url: window.location.origin + '/admin/hoa-don/cap-nhat-trang-thai?id=' + id + '&status=' + stt,
            success: function (data) {
                if (data == 1)
                    alert('Cập nhật trạng thái thành công');
            }
        })
    });

    $(document).on('click', '.delete-item', function () {
        var r = confirm("Bạn muốn xóa danh mục này!");
        if (r == true) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "GET",
                url: window.location.origin + '/admin/hoa-don/xoa/' + id,
                success: function (data) {
                    if (data == 1) {
                        alert('Xóa thành công');
                        window.location.reload();
                    }
                }
            })
        }
    });
</script>