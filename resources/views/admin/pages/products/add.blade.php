
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">Product add</div>
        </div>
        <div class="mt-3">
            @if(count($errors) >0)
                <div class="alert alert-danger">
                    @foreach($errors-> all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-warning">
                    {{session('error')}}
                </div>
            @endif
            
            @if(session('thongbao'))
                <div class="alert alert-danger">
                    {{session('thongbao')}}
                </div>
            @endif
            <form action="{{route('postAddProducts')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <b><label>Name</label></b>
                    <input type="text" class="form-control" name="products_name" />
                </div>
                <div class="form-group">
                    <b><label>Danh mục cha</label></b>
                    <select class="form-control" name="" id="category_parent">
                        <option>Select input</option>
                        @foreach($category as $row)
                            <option class="category_parent" value="{{$row->id}}">{{$row->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <b><label>Danh mục con</label></b>
                    <select class="form-control" name="products_protype" id="category_child">
                        
                    </select>
                </div>

                <div class="form-group">
                    <b><label>Brand</label></b>
                    <select class="form-control" name="products_brand">
                        <option value="0">----Root----</option>
                        @foreach($brand as $row)
                        <option value="{{$row->id}}">{{$row->brand_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <b><label>Price</label></b>
                    <input type="text" class="form-control" name="products_price" />
                </div>
                <div class="form-group">
                    <b><label>Promotion</label></b>
                    <input type="text" class="form-control" name="products_promotion" />
                </div>
                <div class="form-group">
                    <b><label>Image</label></b>
                    <input type="file" name="image" />
                </div>

                 <div class="form-group">
                    <b><label>Details image</label></b>
                    <input type="file" name="image_details[]" multiple/>
                </div>

                <div class="clearfix form-group">
                    <b><label>Information</label></b>
                    
                    <textarea name="products_infor" id="inputTxttxtIntro" class="form-control" rows="2" required="required">{{ old('products_infor') }}</textarea>
                    <script type="text/javascript">
                        var editor = CKEDITOR.replace('products_infor',{
                        language:'vi',
                        filebrowserImageBrowseUrl : '../../../plugin/ckfinder/ckfinder.html?Type=Images',
                        filebrowserFlashBrowseUrl : '../../../plugin/ckfinder/ckfinder.html?Type=Flash',
                        filebrowserImageUploadUrl : '../../../plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl : '../../../plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                        });
                    </script>
                </div>
                </div>

                


                <div>
                    <button type="submit" class="bnt btn-default">Products Add</button>
                    <button type="reset" class="bnt btn-default">Reset</button>
                </div>
            </form>
        </div>
    </div>
    <script
        src="https://code.jquery.com/jquery-1.9.1.js"
        integrity="sha256-e9gNBsAcA0DBuRWbm0oZfbiCyhjLrI6bmqAl5o+ZjUA="
        crossorigin="anonymous"></script>
    <script>
        $( document ).ready(function() {
            $('#category_parent').change(function(){
                var value = $(this).val();
                var base_url = window.origin;
                
                $.ajax({
                    url: base_url + "/admin/san-pham/load-category/" + value,
                    success: function(data){
                        $('#category_child').html(data);
                    }
                });
            });
        });
    </script>
    <script src="{{asset('')}}asset/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@endsection