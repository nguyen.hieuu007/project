
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">Product list</div>
            <a href="{{route('getAddProducts')}}"><button class="btn btn-light">Add Product</button></a>
        </div>
        <div class="d-flex justify-content-between mt-3">
            <div>
                <p style={display: 'inline'}>
                    Show 5 entries</p>
            </div>
            <form style="float:right;margin:5px" action="{{route('search_product')}}" method="get">
                <input type="search"  name="key" class="search_query" placeholder="Tìm kiếm tên sản phẩm">
                <button class="button" type="submit"><span class="fa fa-search"></span></button>
            </form>
        </div>
        @if(session('thongbao'))
            <div class="alert alert-success">
                {{session('thongbao')}}
            </div>
        @endif
        <div class="mt-3">
            <table class="table table-bordered" >
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Bran</th>
                        <th>Price</th>
                        <th>Promition</th>
                        <th>Image</th>
                        <th>Exit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($productsList as $row)
                    <tr align="center">
                        <td>{{$row->id}}</td>
                        <td>{{$row->product_name}}</td>
                        <td>{{$row->brand->brand_name}}</td>
                        <td>{{$row->product_type->name}}</td>
                        <td>{{$row->price}}</td>
                        <td>{{$row->promotion}}</td>
                        <td><div><img src="{{ asset('upload/image/products/'.$row->image) }}" style="width: 100px;" alt="" ></div></td>
                        <td><a href="{{route('getEditProducts',$row->id)}}"><button type="button" class="btn btn-primary btn-sm">Edit</button></a></td>
                        <td><a href="{{route('deleteProducts',$row->id)}}"><button type="button" class="btn btn-primary btn-sm">Delete</button></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
            <div class="d-flex justify-content-end">
                <div class="btn-group">
                {{$productsList->links()}}
                </div>
            </div>
        </div>
    </div>
    
@endsection