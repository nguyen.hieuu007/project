
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">News add</div>
        </div>
        <div class="mt-3">
            @if(count($errors) >0)
                <div class="alert alert-danger">
                    @foreach($errors-> all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif

            @if(session('thongbao'))
                <div class="alert alert-danger">
                    {{session('thongbao')}}
                </div>
            @endif
            <form action="{{route('postAddNews')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <b><label>Name</label></b>
                    <input type="text" class="form-control" name="news_child" />
                </div>
                <div class="form-group">
                    <b><label>Image</label></b>
                    <input type="file" name="image" />
                </div>

                <div class="clearfix form-group">
                    <b><label>Content</label></b>
                    <textarea name="news_content" id="inputTxttxtIntro" class="form-control" rows="2" required="required">{{ old('news_content') }}</textarea>
                    <script type="text/javascript">
                        var editor = CKEDITOR.replace('news_content',{
                        language:'vi',
                        filebrowserImageBrowseUrl : '../../../plugin/ckfinder/ckfinder.html?Type=Images',
                        filebrowserFlashBrowseUrl : '../../../plugin/ckfinder/ckfinder.html?Type=Flash',
                        filebrowserImageUploadUrl : '../../../plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl : '../../../plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                        });
                    </script>
                </div>
                <button type="submit" class="bnt btn-default">News Add</button>
                <button type="reset" class="bnt btn-default">Reset</button>
            </form>
        </div>
    </div>
@endsection