
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">News list</div>
            <a href="{{route('getAddNews')}}"><button class="btn btn-light">Add News</button></a>
        </div>
        <div class="d-flex justify-content-between mt-3">
            <div>
                <p style={display: 'inline'}>
                    Show 5 entries</p>
            </div>
        </div>
        @if(session('thongbao'))
            <div class="alert alert-success">
                {{session('thongbao')}}
            </div>
        @endif
        <div class="mt-3">
            <table class="table table-bordered" >
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Images</th>
                        <th>Content</th>
                        <th>Date</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($newsList as $row)
                    <tr align="center">
                        <td>{{$row->id}}</td>
                        <td>{{$row->article}}</td>
                        <td><img src="{{ asset('upload/image/news/'.$row->image) }}" style="width: 100px;" alt="" ></td>
                        <td>{!!$row->content!!}</td>
                        <td>{{$row->updated_at}}</td>
                        <td><a href="{{route('getEditNews',$row->id)}}"><button type="button" class="btn btn-primary btn-sm">Edit</button></a></td>
                        <td><a href="{{route('getDeleteNews',$row->id)}}"><button type="button" class="btn btn-primary btn-sm">Delete</button></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
            <div class="d-flex justify-content-end">
                <div class="btn-group">
                {{$newsList->links()}}
                </div>
            </div>
        </div>
    </div>
    
@endsection