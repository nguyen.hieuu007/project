
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">Product_type add</div>
        </div>
        <div class="mt-3">
            @if(count($errors) >0)
                <div class="alert alert-danger">
                    @foreach($errors-> all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif

            @if(session('thongbao'))
                <div class="alert alert-danger">
                    {{session('')}}
                </div>
            @endif
            <form action="{{route('postAddProduct_type')}}" method="post">
                @csrf
                <div class="form-group">
                    <b><label>Name</label></b>
                    <input type="text" class="form-control" name="product_type_child" />
                </div>
                <div class="form-group">
                    <b><label>Type</label></b>
                    <select class="form-control" name="type">
                        <option value="0">----Root----</option>
                        @foreach($category as $row)
                        <option value="{{$row->id}}">{{$row->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="bnt btn-default">Product_type Add</button>
                <button type="reset" class="bnt btn-default">Reset</button>
            </form>
        </div>
    </div>
@endsection

