
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">Product_type Exit </div>
        </div>
        <div class="mt-3">
            @if(count($errors) >0)
                <div class='alert alert-danger'>
                    @foreach($errors->all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif

            @if(session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
            @endif
            
            <form action="{{route('postEditProduct_type',$product_typeEdit->id)}}" method="post">
                @csrf
                <div class="form-group">
                    <b><label>Name <small>{{$product_typeEdit->name}}</small></label></b>
                    <input type="text" class="form-control" name="product_type_child" value="{{$product_typeEdit->name}}"/>
                </div>
                <button type="submit" class="bnt btn-default">Product_type Exit</button>
                <button type="reset" class="bnt btn-default">Reset</button>
            </form>
        </div>
    </div>
@endsection