
@extends('admin.master')

@section('Admincontent')

    <div>
        <div class="main-title">
            <div class="text-gray">Product_type list</div>
            <a href="{{route('getAddProduct_type')}}"><button class="btn btn-light">Add Product_type</button></a>
        </div>
        <div class="d-flex justify-content-between mt-3">
            <div>
                <p style={display: 'inline'}>
                    Show 5 entries</p>
            </div>
        </div>
        @if(session('thongbao'))
            <div class="alert alert-success">
                {{session('thongbao')}}
            </div>
        @endif
        <div class="mt-3">
            <table class="table table-bordered" >
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Exit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($product_typeList as $row)
                    <tr align="center">
                        <td>{{$row->id}}</td>
                        <td>{{$row->name}}</td>
                        <td><a href="{{route('getEditProduct_type',$row->id)}}"><button type="button" class="btn btn-primary btn-sm">Edit</button></a></td>
                        <td><a href="{{route('deleteProduct_type',$row->id)}}"><button type="button" class="btn btn-primary btn-sm">Delete</button></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
            <div class="d-flex justify-content-end">
                <div class="btn-group">
                {{$product_typeList->links()}}
                </div>
            </div>
        </div>
    </div>
    
@endsection