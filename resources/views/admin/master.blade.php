<!DOCTYPE html>
<html>

<head>
    @include ('admin.css')
    
</head>

<body>
    <div class="shadow-bottom">
        <div class="">
            <div class="row">
                <div class="col-lg-3 col-6 logo " align="center">
               <!-- <a href="google.com"><img src="{{asset('')}}asset/admin/logo/logo.png" alt="" class="mw-100"/></a> -->
                    <h1>Admin</h1>
                </div>
                <div class="col-lg-9 col-6 menu">
                <!--  -->
                    <h6 style="float: right;"><span class="fas fa-dice-d20"></span>kính chào admin: {{Session::get('user')->name}}</h6>
                </div>
            </div>
        </div>
    </div>
    <!-- ---------menu -->
    <div class="div2">
        <div class="row">
            <div class="left-side col-lg-3 col-6">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('dashboard')}}"><span class="fas fa-diagnoses"></span>
                            Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('category')}}"><span class="fa fa-certificate"></span>
                            Category</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('product_type')}}"><span class="fas fa-dragon"></span>
                            Products Type</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('products')}}"><span class="fab fa-phoenix-framework"></span>
                            Products</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href=""><span class="fas fa-jedi"></span> Image</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href=""><span class="fa fa-clipboard"></span> Bills</a>
                        <ul class="ul2bill">
                            <li><a href="{{route('billCheck')}}">Bill Checked</a></li>
                            <li><a href="{{route('billTemp')}}">Bill not Checked</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('listNews')}}"><span class="fas fa-american-sign-language-interpreting"></span>
                            News</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('listCustomer')}}"><span class="fab fa-accessible-icon"></span>
                        Customer</a>
                    </li>
                    
                    <!-- <li class="nav-item">
                        <a class="nav-link" href=""><span class="fas fa-truck-monster"></span> Slide</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('listUser')}}"><span class=" fas fa-user-cog"></span>
                            Admin</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="background: black;text-align: center;" href="{{route('adminLogout')}}"><span class="fas fa-truck-monster"></span> Log out</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-9 col-6">

                @yield('Admincontent')

            </div>
        </div>
    </div>
</body>

</html>